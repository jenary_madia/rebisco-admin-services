<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();



Route::get('/logout', '\App\Http\Controllers\Auth\LoginController@logout');

Route::group(['middleware' => ['auth']], function () 
{		

	Route::prefix('home')->group(function () {
		Route::get('/', 'HomeController@index')->name('home');
		Route::post('/view-all/{doc_type}', 'HomeController@viewAll');
	});

	Route::prefix('admin')->group(function () {
		Route::get('/dashboard', 'AdminController@index')->name('admin.dashboard');
		Route::post('/view-all/{doc_type}', 'AdminController@viewAll');
	});

	Route::prefix('facilities')->group(function () {
		Route::get('/', 'FacilitiesController@index')->name('facilities.index');
		Route::post('/add', 'FacilitiesController@addFacility');
	});

	Route::prefix('document-logsheet')->group(function () {
		Route::post('/save', 'DocumentLogsheetController@store');
		Route::post('/filter/{type}', 'DocumentLogsheetController@filter');
		Route::post('/receive', 'DocumentLogsheetController@receive');
		Route::get('/transmital', 'DocumentLogsheetController@transmitalLog')->name('document-logsheet.transmital');
		Route::post('/pdf/download', 'DocumentLogsheetController@download')->name('document-logsheet.download');
		Route::post('/delete', 'DocumentLogsheetController@delete');
	});

	Route::prefix('documents')->group(function () {
		Route::get('/{type?}', 'DocumentLogsheetController@index')->name('document-logsheet.index');
	});

	Route::prefix('events')->group(function () {
	    Route::get('/', 'EventsController@index')->name('events.index');
	    Route::post('/add', 'EventsController@store');
	    Route::get('/send-mail', 'EventsController@sendInvite');
	});
	
	Route::prefix('event')->group(function () {
	    Route::get('/{id}', 'EventsController@show');
	    Route::post('/{id}', 'EventsController@update');
	    Route::post('/{id}/download-attendees', 'EventsController@download');
	    Route::post('/{id}/add-comment', 'EventsController@addComment');
	    Route::post('/{id}/confirm-attendance', 'EventsController@confirmAttendance');
	});

	Route::prefix('facility')->group(function () {
		Route::post('/{id}', 'FacilitiesController@updateFacility');
	});

	Route::get('/download/{pathToFile}/{originalName}',function($pathToFile,$originalName) {
		return response()->download(public_path("storage\\uploads")."\\".$pathToFile,$originalName);
		// return asset("storage/$pathToFile");
	})->name('download');

	Route::post('/file-upload', 'FileController@upload');

	//ESS
	Route::prefix('ess')->group(function () {
	    Route::get('/dashboard', 'EssController@index')->name("ess.dashboard");
	    Route::get('/document/request', 'EssController@requestDocument')->name("ess.document.request");
	    Route::get('/document/{id}', 'EssController@document');
	    Route::post('/document/request', 'Ess\DocumentController@store');
	    Route::post('/document/send', 'Ess\DocumentController@send');
	});

});