@extends('layouts.app')

@section('content')
    <div class="panel panel-default panel-as">
        <div class="panel-heading">Dashboard</div>
        <div class="panel-body">
            <div class="col-md-12">
                <h3>Documents for storage disposal.</h3>
                <table class="table table-bordered">
                    <thead class="table-header">
                        <tr>
                            <th>NAME</th>
                            <th>DEPARTMENT</th>
                            <th>REQUEST TYPE</th>
                            <th>ACTION</th>
                        </tr>
                    </thead>
                    <tbody class="text-center">
                        <tr>
                            <td>SUPPLIER</td>
                            <td>BUDGET</td>
                            <td>ACTUAL</td>
                            <td>
                                <button class="container-icon">
                                    <i class="glyphicon glyphicon-remove"></i>
                                </button>
                                <button class="container-icon">
                                    <i class="glyphicon glyphicon-pencil"></i>
                                </button>
                            </td>
                        </tr>
                    </tbody>
                </table>
                <button class="btn btn-danger btn-sm">VIEW ALL</button>
                <br>
                <h3>Documents to be received. <br><small>{{ ucfirst(strtolower(Auth::user()->firstname)) }}, pending requests and documents are listed here</small></h3>
                <table class="table table-bordered" id="list_incoming">
                    <thead class="table-header">
                        <tr>
                            <th>DATE</th>
                            <th>TIME</th>
                            <th>ORIGIN</th>
                            <th>ACTION</th>
                        </tr>
                    </thead>
                    <tbody class="text-center">
                        @foreach($logsheets as $logsheet)
                            <tr>
                                <td>{{ $logsheet->ls_date_sent }}</td>
                                <td>{{ $logsheet->ls_time_sent }}</td>
                                <td>{{ $logsheet->text }}</td>
                                <td>
                                    <a style="text-align:center; display : inline-block; border : 1px solid black;" class="container-icon" href="{{ route('document-logsheet.index','incoming') }}">
                                        <div class="icon-view"></div>
                                    </a>
                                </td>
                            </tr>
                        @endforeach
                    </tbody>
                </table>
                <button id="view-all-incoming" type="button" class="btn btn-danger btn-sm">VIEW ALL</button>
            </div>
        </div>
    </div>
@endsection
@section("scripts")
    <script type="text/javascript">
        var incoming_documents = $('#list_incoming').DataTable({
            "bLengthChange": false,
            "lengthMenu": [[10, 15, -1], [10, 15, "All"]],
            "info": false,
            "searching": false,
            "bPaginate":false,
        });


       

        $('body').on("click","#view-all-incoming" ,function(e) {
            e.preventDefault();
            axios.post(base_url+'/admin/view-all/incoming', {})
            .then(function (response) {
                var data = response.data;
                if(data.length > 10) {
                     incoming_documents.destroy();
                    incoming_documents = $('#list_incoming').DataTable({
                        "bLengthChange": false,
                        "lengthMenu": [[10, 15, -1], [10, 15, "All"]],
                        "info": false,
                        "searching": false,
                        "bPaginate":true,
                    });
                }
                
                incoming_documents.clear()
                var to_display = [];
                for (var i = 0; i < data.length; i++) {
                    to_display.push([
                        data[i]["ls_date_sent"],
                        data[i]["ls_time_sent"],
                        data[i]["text"],
                        `<a style="text-align:center; display : inline-block; border : 1px solid black;" class="container-icon" 
                        href="`+base_url+'/documents/incoming'+`"
                        ><div class="icon-view"></div></a>`,
                    ])
                }
                incoming_documents.rows.add(to_display).draw().nodes();
                
            })
            .catch(function (error) {
            });
        })
    </script>
@endsection
