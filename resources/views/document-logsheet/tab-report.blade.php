<div class="panel-body">
    <div class="col-md-12">
        <span class="pull-left page-title">Report</span>
    </div>
    <div class="col-md-12">
        <span class="sub-header">Filter Documents</span>
        <hr class="hr-no-margin">
        <br>
        @include("document-logsheet.filter",[
            'form_id' => 'filter_form_report',
            'btn_id' => '',
            'form_action' => route('document-logsheet.download')
        ])
        <br>
        <div style="overflow-x: scroll; min-height: 350px;" >
            <table class="table table-bordered" id="list_report">
                <thead class="table-header">
                    <tr>
                        <th>NAME</th>
                        <th>DEPARTMENT</th>
                        <th>PARTICULARS</th>
                        <th style="background-color: red;">RECIPIENT</th>
                        <th style="background-color: red;">DEPARTMENT</th>
                        <th>RECEIVED</th>
                    </tr>
                </thead>
                <tbody id="body_list_report">
                    @foreach($report_logs as $log)
                        <tr>
                            <td>
                                <input value='{{ $log->sender_fullname }}' type='text' class='form-control input-sm' readonly>
                            </td>
                            <td>
                                <input value='{{ $log->sender_dept }}' type='text' class='form-control input-sm' readonly>
                            </td>
                            <td>
                                <input value='{{ $log->ls_particulars }}' type='text' class='form-control input-sm' readonly>
                            </td>
                            <td>
                                <input value='{{ $log->recipient_fullname }}' type='text' class='form-control input-sm' readonly>
                            </td>
                            <td>
                                <input value='{{ $log->recipient_dept }}' type='text' class='form-control input-sm' readonly>
                            </td>
                            <td>
                                <input value='{{ $log->receiver_fullname }}' type='text' class='form-control input-sm' readonly>
                            </td>
                        </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
    </div>
    <div class="col-md-12 text-center">
            <button class="btn btn-warning btn-sm" id="btn-download-report">DOWNLOAD</button>
    </div>
</div>