@extends('layouts.app')

@section('content')
    <div class="panel panel-default panel-as">
        <div class="panel-heading">Document Logsheet</div>
        <div class="sub-panel-heading">
        	<ul class="tab-head-document list-inline text-center">
        		<li class="{{ $type == 'outgoing' ? 'active' : '' }}" ><a href="#outgoing" aria-controls="outgoing" role="tab" data-toggle="tab">Outgoing</a></li>
        		<li class="{{ $type == 'incoming' ? 'active' : '' }}" ><a href="#incoming" aria-controls="incoming" role="tab" data-toggle="tab">Incoming</a></li>
        		<li class="{{ $type == 'report' ? 'active' : '' }}" ><a href="#report" aria-controls="report" role="tab" data-toggle="tab">Report</a></li>
	        </ul>
	    </div>
        <div class="tab-content">
            <div role="tabpanel" class="tab-pane {{ $type == 'outgoing' ? 'active' : 'fade' }}" id="outgoing">
                @include("document-logsheet.tab-outgoing")
            </div>
            <div role="tabpanel" class="tab-pane {{ $type == 'incoming' ? 'active' : 'fade' }}" id="incoming">
                @include("document-logsheet.tab-incoming")
            </div>
            <div role="tabpanel" class="tab-pane {{ $type == 'report' ? 'active' : 'fade' }}" id="report">
                @include("document-logsheet.tab-report")
            </div>
        </div>
    </div>

    <div>
    </div>

@endsection
@section('scripts')
    <script>
        window.employees = '{!! $employees !!}';
        window.outgoing_logs = '{!! $outgoing_logs !!}';
        $('#btn-download-report').on('click',function(e) {
            e.preventDefault();
            $('#filter_form_report').submit(); 
        });
        $("select[name='ls_origin']").on("change",function() {
            var val = $(this).val();

            if(val == $('select[name="ls_destination"]').val())
                $('select[name="ls_destination"]').val("");

            if ($(this).val() != "") {
                $('select[name="ls_destination"]').find('option').attr("disabled",false);
                $('select[name="ls_destination"]').find('option[value="'+val+'"]').attr("disabled",true);
            }else{
                $(this).find('option').attr("disabled",false);
                $('select[name="ls_destination"]').find('option').attr("disabled",false);
            }
           
        });

        $("select[name='ls_destination']").on("change",function() {
            var val = $(this).val();

            if(val == $('select[name="ls_origin"]').val() && $('select[name="ls_origin"]').val())
                $('select[name="ls_origin"]').val("");

            if ($(this).val() != "") {
                $('select[name="ls_origin"]').find('option').attr("disabled",false);
                $('select[name="ls_origin"]').find('option[value="'+val+'"]').attr("disabled",true);
            }else{
                $(this).find('option').attr("disabled",false);
                $('select[name="ls_origin"]').find('option').attr("disabled",false);
            }

            
        });
    </script>
    <script src="{{ asset('js/documents.js') }}"></script>
@endsection
