<div class="panel-body">
    <div class="col-md-12">
        <span class="pull-left page-title">Incoming Documents</span>
    </div>
    <div class="col-md-12">
        <span class="sub-header">Filter Documents</span>
        <hr class="hr-no-margin">
        <br>
        @include("document-logsheet.filter",[
            'form_id' => 'filter_form_incoming',
            'btn_id'=>'btn-filter-incoming',
            'btn_name'=>'FILTER'
        ])
        <br>
        <div style="overflow-x: scroll; min-height: 350px;" >
            <table id="list_incoming" class="table table-bordered text-center">
                <thead class="table-header">
                    <tr>
                        <th>NAME</th>
                        <th>DEPARTMENT</th>
                        <th>PARTICULARS</th>
                        <th style="background-color: red;">RECIPIENT</th>
                        <th style="background-color: red;">DEPARTMENT</th>
                        <th>RECEIVED</th>
                    </tr>
                </thead>
                <tbody id="body_list_incoming">
                    @foreach($incoming_logs as $log)
                        <tr>
                            <td>
                                <input value='{{ $log->sender_fullname }}' type='text' class='form-control input-sm' readonly>
                            </td>
                            <td>
                                <input value='{{ $log->sender_dept }}' type='text' class='form-control input-sm' readonly>
                            </td>
                            <td>
                                <input value='{{ $log->ls_particulars }}' type='text' class='form-control input-sm' readonly>
                            </td>
                            <td>
                                <input value='{{ $log->receiver_fullname }}' type='text' class='form-control input-sm' readonly>
                            </td>
                            <td>
                                <input value='{{ $log->receiver_dept }}' type='text' class='form-control input-sm' readonly>
                            </td>
                            <td>
                                <input class="incoming_cbox" type="checkbox" value="{{ $log->ls_id }}">
                            </td>
                        </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
    </div>
    <div class="col-md-12 text-center">
        <button style="display:none;" class="btn btn-warning btn-sm btn-receive-incoming">SAVE</button>
    </div>
</div>