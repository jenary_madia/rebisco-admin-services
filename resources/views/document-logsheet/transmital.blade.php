@extends('layouts.app')

@section('content')
    <div class="panel panel-default panel-as">
        <div class="panel-heading">Facilities Management</div>
        <div class="sub-panel-heading">
        	<ul class="list-inline">
        		<li><a href="{{ route('document-logsheet.index','outgoing') }}">< Return to Outgoing Documents</a></li>
	        </ul>
	    </div>
        <div class="panel-body">
            <div class="col-md-12">
                <br>    
                <br>    
                <div style="overflow-x: scroll; min-height: 350px;" >
                    <table id="list_incoming" class="table table-bordered">
                        <thead class="table-header">
                            <tr>
                                <th>FROM</th>
                                <th>PARTICULARS</th>
                                <th>TO</th>
                                <th>DEPARTMENT/COMPANY</th>
                                <th>RECEIVED BY</th>
                            </tr>
                        </thead>
                        <tbody id="body_list_incoming">
                            @foreach($transmital_logs as $log)
                                <tr>
                                    <td>
                                        <input value='{{ $log->sender_fullname }}' type='text' class='form-control input-sm' readonly>
                                    </td>
                                    <td>
                                        <input value='{{ $log->ls_particulars }}' type='text' class='form-control input-sm' readonly>
                                    </td>
                                    <td>
                                        <input value='{{ $log->recipient_fullname }}' type='text' class='form-control input-sm' readonly>
                                    </td>
                                    <td>
                                        <input value='{{ $log->recipient_dept }}' type='text' class='form-control input-sm' readonly>
                                    </td>
                                    <td>
                                        <input value='{{ $log->receiver_fullname }}' type='text' class='form-control input-sm' readonly>
                                    </td>
                                </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
            <div class="col-md-12 text-center">
                <button style="display:none;" class="btn btn-warning btn-sm btn-receive-incoming">SAVE</button>
            </div>
        </div>
    </div>

    <div>
    </div>

@endsection
@section('scripts')
        
@endsection
