@if($form_id == "filter_form_report")
    <form id="filter_form_report" class="form-inline" action="{{ isset($form_action) ? $form_action : '' }}" method="POST">
        <input type="hidden" name="_token" value="{{ csrf_token() }}">
        <div class="form-group">
            <label class="text-uppercase required" for="exampleInputName2">Document Origin</label>
            <select name="ls_origin" class="form-control input-sm" style="width : 100px; margin-left : 10px; margin-right : 10px" id="ls_origin">
                <option value="" selected disabled></option>
                <option value="">ALL</option>
                @foreach($locations as $loc)
                    <option value="{{ $loc->item }}">{{ $loc->text }}</option>
                @endforeach
            </select>
        </div>
        <div class="form-group">
            <label class="text-uppercase required">Destination</label>
            <select name="ls_destination" class="form-control input-sm" style="width : 100px; margin-left : 10px; margin-right : 10px">
                <option value="" selected disabled></option>
                <option value="">ALL</option>
                @foreach($locations as $loc)
                    <option value="{{ $loc->item }}">{{ $loc->text }}</option>
                @endforeach
            </select>
        </div>
        <div class="form-group">
            <label class="text-uppercase required">Date from</label>
            <div class="input-group" style="margin-right : 10px">
                <input name="ls_date_from" style="width : 85px; margin-left : 10px" type="text" class="form-control input-sm datepicker">
                <span class="input-group-addon" ><i class="glyphicon glyphicon-calendar"></i></span>
            </div>
        </div>
        <div class="form-group">
            <label class="text-uppercase required">Date to</label>
            <div class="input-group" style="margin-right : 10px">
                <input  name="ls_date_to" style="width : 70px; margin-left : 10px;" type="text" class="form-control input-sm datepicker" value="">
                <span class="input-group-addon" ><i class="glyphicon glyphicon-calendar"></i></span>
            </div>
        </div>
    </form>
@else
    <form id="{{ $form_id }}" class="form-inline" action="{{ isset($form_action) ? $form_action : '' }}" method="POST">
        <input type="hidden" name="_token" value="{{ csrf_token() }}">
        <div class="form-group">
            <label class="text-uppercase required" for="exampleInputName2">Document Origin</label>
            <select name="ls_origin" class="form-control input-sm" style="width : 100px; margin-left : 10px; margin-right : 10px" id="ls_origin">
                <option value="" selected></option>
                @foreach($locations as $loc)
                    <option value="{{ $loc->item }}">{{ $loc->text }}</option>
                @endforeach
            </select>
        </div>
        <div class="form-group">
            <label class="text-uppercase required">Destination</label>
            <select name="ls_destination" class="form-control input-sm" style="width : 100px; margin-left : 10px; margin-right : 10px">
                <option value="" selected></option>
                @foreach($locations as $loc)
                    <option value="{{ $loc->item }}">{{ $loc->text }}</option>
                @endforeach
            </select>
        </div>
        <div class="form-group">
            <label class="text-uppercase required">Date sent</label>
            <div class="input-group" style="margin-right : 10px">
                <input name="ls_date_sent" style="width : 85px; margin-left : 10px" type="text" class="form-control input-sm datepicker">
                <span class="input-group-addon" ><i class="glyphicon glyphicon-calendar"></i></span>
            </div>
        </div>
        <div class="form-group">
            <label class="text-uppercase required">Time</label>
            <div class="input-group" style="margin-right : 10px">
                <input  name="ls_time_sent" style="width : 70px; margin-left : 10px;" type="text" class="form-control input-sm timepicker" value="">
                <span class="input-group-addon" ><i class="glyphicon glyphicon-time"></i></span>
            </div>
        </div>
        <button type="button" id="{{ $btn_id }}" class="btn btn-danger btn-sm pull-right">{{ $btn_name }}</button>
    </form>
@endif
