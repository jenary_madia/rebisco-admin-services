<div class="panel-body">
    <div class="col-md-12">
        <span class="pull-left page-title">Outgoing Documents</span>
        <button id="btn-add-outgoing" class="btn btn-danger btn-sm pull-right">ADD DOCUMENT</button>
    </div>
    <div class="col-md-12">
        <span class="sub-header">Filter Documents</span>
        <hr class="hr-no-margin">
        <br>
        @include("document-logsheet.filter",[
            'form_id' => 'filter_form',
            'btn_id' => 'btn-filter-outgoing',
            'btn_name'=>'VIEW'
        ])
        <br>
        <div style="overflow-x: scroll; min-height: 350px;" >
            <table id="list_outgoing" class="table table-bordered text-center">
                <thead class="table-header">
                    <tr>
                        <th>NAME</th>
                        <th>DEPARTMENT</th>
                        <th>PARTICULARS</th>
                        <th style="background-color: red;">RECIPIENT</th>
                        <th style="background-color: red;">DEPARTMENT</th>
                        <th>ACTION</th>
                    </tr>
                </thead>
                <tbody id="body_list_outgoing">
                </tbody>
            </table>
        </div>
    </div>
    <div class="col-md-12 text-center">
        <button style="display:none;" class="btn btn-warning btn-sm btn-save-outgoing">SAVE</button>
        <button style="display:none;" class="btn btn-warning btn-sm btn-reset-outgoing">RESET</button>
        <a href="{{ route('document-logsheet.transmital') }}" class="btn btn-warning btn-sm">VIEW TRANSMITAL LOG</a>
    </div>
</div>