@extends('layouts.app')

@section('content')
    <div class="panel panel-default panel-as">
        <div class="panel-heading">Facilities Management</div>
        <div class="sub-panel-heading">
            <ul class="list-inline text-center">
                <li><a href="#furnitures-fixtures" aria-controls="furnitures-fixtures" role="tab" data-toggle="tab">Furnitures and Fixtures</a></li>
                <li><a href="#create-event" aria-controls="create-event" role="tab" data-toggle="tab">Document</a></li>
            </ul>
        </div>
        
        <div class="tab-content">
            <div role="tabpanel" class="tab-pane active" id="furnitures-fixtures">
                @include("facilities.tab-furniture-fixture")
            </div>
            <div role="tabpanel" class="tab-pane fade" id="create-event">
                @include("facilities.tab-list-documents")
            </div>
        </div>
    </div>

    <div>
    </div>

@endsection
@section('scripts')
    <script  src="{{ asset('js/furnitures-fixtures.js') }}"></script>
@endsection
