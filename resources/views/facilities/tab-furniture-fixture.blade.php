<div class="panel-body">
    <h3>Add Furnitures or Fixtures</h3>
    <div class="div-add-furniture">
        <form id="add-facility">
            <div class="col-md-9">
                <div class="form-horizontal">
                    <div class="form-group">
                        <label class="col-sm-2 control-label">Name</label>
                        <div class="col-sm-10">
                            <input type="text" maxlength="150" id="fa_name" name="fa_name" class="form-control input-sm">
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="inputPassword3" class="col-sm-2 control-label">Type</label>
                        <div class="col-sm-10">
                            <select id="fa_type" name="fa_type" class="form-control input-sm">
                                <option></option> 
                                @foreach($types as $key => $value)                               
                                    <option value="{{ $value }}">{{ $key }}</option>                                
                                @endforeach
                            </select>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-2 control-label">Acquisition date</label>
                        <div class="col-sm-4">
                            <input type="text" id="fa_acquisition_date" id="fa_acquisition_date" name="fa_acquisition_date" class="form-control input-sm datepicker">
                        </div>
                        <label for="inputPassword3" class="col-sm-2 control-label">Retirement date</label>
                        <div class="col-sm-4">
                            <input type="text" id="fa_retirement_date" id="fa_retirement_date" name="fa_retirement_date" class="form-control input-sm datepicker">
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-2 control-label">Site</label>
                        <div class="col-sm-4">
                            <select id="fa_site" name="fa_site" class="form-control input-sm">
                                <option selected disabled></option>
                                @foreach($sites as $key => $value)
                                    <option value="{{ $key }}">{{ $value }}</option>
                                @endforeach
                            </select>
                        </div>
                        <label for="inputPassword3" class="col-sm-2 control-label">Area</label>
                        <div class="col-sm-4">
                            <select id="fa_area" name="fa_area" class="form-control input-sm">
                                <option selected disabled></option>
                                @foreach($areas as $key => $value)
                                    <option value="{{ $value }}">{{ $key }}</option>
                                @endforeach
                                <option value="others">Others</option>
                            </select>
                            <br>
                            <input style="display: none;" type="text" name="other_area" id="other_area" placeholder="Enter Other area here">
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-3 div-asset-number">
                <div class="form-group">
                    <label for="exampleInputEmail1">Asset Number</label>
                    <input type="text" id="fa_asset_number" maxlength="20" name="fa_asset_number" class="form-control input-sm" id="exampleInputEmail1">
                </div>
            </div>
            <button type="submit" class="btn btn-danger btn-sm btn-save">SAVE</button>
            <button type="button" style="display:none" class="btn btn-danger btn-sm btn-update">SAVE</button>
            <button id="reset-facility" class="btn btn-default btn-sm">RESET</button>
        </form>
    </div>
    <div class="clearfix"></div>
    <br>
    <div class="col-md-12">
        <h3>List of furnitures and fixtures. <br><small class="sub-header">Your pendng requests and documents are listed here</small></h3>
        <!-- <form class="form-inline">
            <div class="form-group">
                <label for="exampleInputName2">Name</label>
                <select type="text" class="form-control input-sm" style="width : 120px; margin-left : 10px; margin-right : 10px" id="exampleInputName2"></select>
            </div>
            <div class="form-group">
                <label for="exampleInputEmail2">Email</label>
                <select type="email" class="form-control input-sm" style="width : 120px; margin-left : 10px; margin-right : 10px" id="exampleInputEmail2"></select>
            </div>
            <div class="form-group">
                <label for="exampleInputEmail2">Email</label>
                <select type="email" class="form-control input-sm" style="width : 120px; margin-left : 10px; margin-right : 10px" id="exampleInputEmail2"></select>
            </div>
            <button type="submit" class="btn btn-danger btn-sm">FILTER</button>
            <div class="form-group pull-right">
                <input type="email" class="form-control input-sm" id="exampleInputEmail2">
            </div>
        </form> -->
        <br>
        <table id="list-furnitures" class="table table-bordered text-center">
            <thead class="table-header">
                <tr>
                    <th>NAME</th>
                    <th>SITE</th>
                    <th>TYPE</th>
                </tr>
            </thead>
            <tbody>
                @foreach($facilities as $facility)
                    <tr class="facility_row" data-json='{!! $facility !!}'>
                        <td>{{ $facility->fa_name }}</td>
                        <td>{{ $facility->fa_site }}</td>
                        <td>{{ $facility->type->text }}</td>
                    </tr>
                @endforeach
            </tbody>
        </table>
    </div>
</div>