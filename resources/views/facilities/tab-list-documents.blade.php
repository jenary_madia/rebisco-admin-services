<div class="panel-body">
    <div class="col-md-12">
        <span class="pull-left page-title">Documents</span>
    </div>
    <div class="col-md-12">
        <span class="sub-header">Filter Documents</span>
        <hr class="hr-no-margin">
        <br>
        <form class="form-inline">
            <div class="form-group">
                <label class="text-uppercase required" for="exampleInputName2">YEAR</label>
                <select type="text" class="form-control input-sm" style="width : 100px; margin-left : 10px; margin-right : 10px" id="exampleInputName2"></select>
            </div>
            <div class="form-group">
                <label class="text-uppercase required" for="exampleInputName2">SITE</label>
                <select type="text" class="form-control input-sm" style="width : 100px; margin-left : 10px; margin-right : 10px" id="exampleInputName2"></select>
            </div>
            <div class="form-group">
                <label class="text-uppercase required" for="exampleInputName2">TYPE</label>
                <select type="text" class="form-control input-sm" style="width : 100px; margin-left : 10px; margin-right : 10px" id="exampleInputName2"></select>
            </div>
            <button type="submit" class="btn btn-danger btn-sm">FILTER</button>
            <div class="form-group">
                <input style="width : 100px; margin-left : 10px; margin-right : 10px" type="email" class="form-control input-sm" placeholder="Search">
            </div>
        </form>
        <br>
        <table id="list-documents" class="table table-bordered">
            <thead class="table-header">
                <tr>
                    <th>EVENT NAME</th>
                    <th>DATE</th>
                    <th>TOTAL EXPENSES</th>
                    <th>ACTION</th>
                </tr>
            </thead>
            <tbody class="text-center">
                @foreach($documents as $document)
                    <tr>
                        <td>{{ $document->owner->fullname }}</td>
                        <td>{{ $document->d_site }}</td>
                        <td>{{ $document->d_request_type == 1 ? "Storage" : "Disposal" }}</td>
                        <td>
                            <button class="container-icon">
                                <i class="glyphicon glyphicon-remove"></i>
                            </button>
                            <a class="container-icon" href="{{ url('/ess/document',$document->d_id) }}">
                                <i class="glyphicon glyphicon-pencil" ></i>
                            </a>
                        </td>
                    </tr>
                @endforeach
            </tbody>
        </table>
    </div>
</div>