@auth
    <div class="col-md-2">
        <div class="panel panel-default panel-as">
            @if(Request::route()->getPrefix() == "/ess")
                <div class="panel-heading">Employee Self Service</div>

                <div class="panel-body div-sidebar">
                    <ul class="list-unstyled sidebar-list">
                        <li>
                            <a href="{{ route('ess.dashboard') }}">Dashboard</a>
                        </li>
                        <li>
                            <a href="{{ url('/ess/document/request') }}">Document Request</a>
                        </li>
                        </li>
                    </ul>
                </div>
            @else
                <div class="panel-heading">Admin</div>

                <div class="panel-body div-sidebar">
                    <ul class="list-unstyled sidebar-list">
                        <li style="{{ Request::route()->getName() == "admin.dashboard" ? 'background-color : #f4f4f4' : ''}}">
                            <a href="{{ route('admin.dashboard') }}">Dashboard</a>
                            @if(Request::route()->getName() == "admin.dashboard")
                                <i class="glyphicon glyphicon-chevron-right pull-right"></i>
                            @endif
                        </li>
                        <li style="{{ Request::route()->getName() == "document-logsheet.index" ? 'background-color : #f4f4f4' : ''}}">
                            <a href="{{ route('document-logsheet.index','outgoing') }}">Document Logsheet</a>
                            @if(Request::route()->getName() == "document-logsheet.index")
                                <i class="glyphicon glyphicon-chevron-right pull-right"></i>
                            @endif
                        </li>
                        <li style="{{ Request::route()->getName() == "events.index" ? 'background-color : #f4f4f4' : ''}}">
                            <a href="{{ route('events.index') }}">Company Events</a>
                            @if(Request::route()->getName() == "events.index")
                                <i class="glyphicon glyphicon-chevron-right pull-right"></i>
                            @endif
                        </li>
                        <li style="{{ Request::route()->getName() == "facilities.index" ? 'background-color : #f4f4f4' : ''}}">
                            <a href="{{ route('facilities.index') }}">Facilities</a>
                            @if(Request::route()->getName() == "facilities.index")
                                <i class="glyphicon glyphicon-chevron-right pull-right"></i>
                            @endif
                        </li>
                    </ul>
                </div>
            @endif
        </div>
    </div>
@endauth