<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}"  id="token">
    <meta name="csrf-url" content="{{ url('/') }}" id="base-url">

    <title>{{ config('app.name', 'Laravel') }}</title>

    <!-- Styles -->
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
    <link href="{{ asset('css/main.css') }}" rel="stylesheet">
    <link rel="stylesheet" href="http://code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
    <link href="{{ asset('css/datatables.min.css') }}" rel="stylesheet">
    <link href="{{ asset('css/chosen.min.css') }}" rel="stylesheet">
    <link href="{{ asset('css/bootstrap-timepicker.min.css') }}" rel="stylesheet">
</head>
<body>
    <div id="app">
        <nav class="navbar navbar-default navbar-static-top top-bar">
            <div class="container-fluid">
                <div class="navbar-header">

                    <!-- Collapsed Hamburger -->
                    <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#app-navbar-collapse">
                        <span class="sr-only">Toggle Navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>

                    <!-- Branding Image -->
                    <a class="navbar-brand" href="{{ url('/') }}">
                        WEDNESDAY, SEPTEMBER 20, 2017
                    </a>
                </div>

                <div class="collapse navbar-collapse" id="app-navbar-collapse">
                    <!-- Left Side Of Navbar -->
                    <ul class="nav navbar-nav">
                        &nbsp;
                    </ul>

                    <!-- Right Side Of Navbar -->
                    <div class="nav navbar-nav navbar-right">
                        <!-- Authentication Links -->
                        @auth
                            <div class="pull-left">
                                <div class="pull-left">
                                    <span class="pull-right">WELCOME! JUAN DELA CRUZ</span>
                                    <br>
                                    <span class="pull-right">CHRD ADMIN</span>
                                </div>
                            </div>
                            <img style="margin-left: 10px; margin-top: 5px; border-radius: 25px;"class="pull-right" src="http://via.placeholder.com/40x40">
                        @endif
                    </div>

                </div>
            </div>
        </nav>
        <div class="top-logo-div">
            <img class="pull-left" style="margin-left: 10px; margin-top: 5px; border-radius: 25px; width : 100px;" src="{{ asset('/images/rebisco-logo.png') }}">
            <div class="pull-left"> 
                HUMAN RESOURCE MANAGEMENT SYSTEM
            </div>
        </div>
        <nav class="navbar navbar-default navbar-static-top">
            <div class="container-fluid">
                <div class="navbar-header">

                    <!-- Collapsed Hamburger -->
                    <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#app-navbar-collapse">
                        <span class="sr-only">Toggle Navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                </div>

                <div class="collapse navbar-collapse" id="app-navbar-collapse">
                    @auth
                    <!-- Left Side Of Navbar -->
                    <ul class="nav navbar-nav" style="font-size: 12px;">
                        <li><a href="{{ route('login') }}">Home</a></li>
                        <li><a href="{{ route('register') }}">Recruitment</a></li>
                        <li><a href="{{ route('register') }}">Employee Information</a></li>
                        <li><a href="{{ route('register') }}">Timekeeping</a></li>
                        <li><a href="{{ route('register') }}">Payroll</a></li>
                        <li><a href="{{ route('register') }}">Training</a></li>
                        <li><a href="{{ route('register') }}">Organizational Development</a></li>
                        <li><a href="{{ route('admin.dashboard') }}">Admin</a></li>
                        <li><a href="{{ route('ess.dashboard') }}">Employee Self Service</a></li>
                    </ul>

                    <!-- Right Side Of Navbar -->
                    <ul class="nav navbar-nav navbar-right" style="font-size: 14px;">
                        <!-- Authentication Links -->
                        <li><a href="{{ route('register') }}">Notification</a></li>
                        <li><a href="{{ route('logout') }}">Logout</a></li>
                    </ul>
                    @endauth
                </div>
            </div>
        </nav>
        <div class="container main-content">
            <div class="row">
               @if(Request::route()->getName() != 'home')
                    @include("layouts.sidebar")
                    <div class="col-md-10">
                         @yield('content')
                    </div>
                @else
                    @yield('content')
                @endif
            </div>
        </div>
        <div class="footer">
            <div class="top">
                <div class="container-fluid">
                    Home    
                </div>
            </div>

            <div class="bottom">
                <div class="container-fluid">
                    <ul class="list-unstyled pull-left">
                        <li class="highlight">&copy; HUMAN RESOURCE MANAGEMENT SYSTEM</li>
                        <li>Republic Biscuit Corporation</li>
                    </ul>
                    <ul class="list-unstyled pull-left">
                        <li>Home</li>
                        <li>Recruitment</li>
                        <li>Employee Information</li>
                        <li>Timekeeping</li>
                        <li>Payroll</li>
                        <li>Training</li>
                        <li>Organizational Development</li>
                        <li>Admin</li>
                        <li>Employee Self Service</li>
                    </ul>
                    <ul class="list-unstyled pull-left">
                        <li class="highlight">Quick Links</li>
                        <li>Add Employee</li>
                        <li>Add Outgoing Documents</li>
                        <li>Add Event</li>
                    </ul>
                    <ul class="list-unstyled pull-left">
                        <li class="highlight">Self Help</li>
                        <li>How to Use</li>
                    </ul>
                </div>
            </div>
        </div>
    </div>

    <!-- Scripts -->
    <script src="{{ asset('js/app.js') }}"></script>
    <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
    <script src="{{ asset('js/datatables.min.js') }}"></script>
    <script src="{{ asset('js/chosen.jquery.min.js') }}"></script>
    <script src="{{ asset('js/bootstrap-timepicker.min.js') }}"></script>
    @yield('scripts')
</body>
</html>
