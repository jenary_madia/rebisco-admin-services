@extends('layouts.app')

@section('content')
	<div class="panel panel-default panel-as">
	    <div class="panel-heading">Facilities Management</div>
        <div class="panel-body">
		    <div class="div-add-furniture">
		        <form id="add-facility">
		            <div class="col-md-8">
		                <div class="form-horizontal">
		                    <div class="form-group">
		                        <label class="col-sm-2 control-label">Request No</label>
		                        <div class="col-sm-10">
		                            <input type="text" value="{{ $document->d_request_no }}" id="d_request_no" name="d_request_no" class="form-control input-sm">
		                        </div>
		                    </div>
		                    <div class="form-group">
		                        <label for="inputPassword3" class="col-sm-2 control-label">Name</label>
		                        <div class="col-sm-10">
		                            <input type="text" id="owner" name="owner" value="{{ $document->owner->fullname }}" class="form-control input-sm" id="inputPassword3">
		                        </div>
		                    </div>
		                    <div class="form-group">
		                        <label class="col-sm-2 control-label">Department</label>
		                        <div class="col-sm-4">
		                            <input type="text" value="{{ $document->owner->dept_name }}" class="form-control input-sm">
		                        </div>
		                        <label class="col-sm-2 control-label">Site</label>
		                        <div class="col-sm-4">
		                            <select id="site" class="form-control input-sm">
		                            	@foreach($sites as $key => $value)
		                            		<option value="{{ $key }}">{{ $value }}</option>
		                            	@endforeach
		                            </select>
		                        </div>
		                    </div>
		                    <div class="form-group">
		                        <label class="col-sm-2 control-label">Request Type</label>
		                        <div class="col-sm-4">
		                            <input type="text" value="{{ $document->d_request_type == 1 ?  'Storage' : 'Disposal' }}" class="form-control input-sm">
		                        </div>
		                    </div>
		                    <div class="form-group">
		                        <label class="col-sm-2 control-label">Description</label>
		                        <div class="col-sm-8">
		                            <textarea class="form-control input-sm">{{ $document->d_description }}</textarea>
		                        </div>
		                    </div>
		                </div>
		            </div>
		            @if($document->d_status == 2)
			            <div class="col-md-4 div-asset-number">
			                <div class="form-group">
			                    <label for="exampleInputEmail1">CHRD Remarks</label>
			                    <textarea id="chrd_remarks" type="text" class="form-control input-sm"></textarea>
			                </div>
			                <div class="form-group">
		                        <label control-label">Received Date</label>
	                            <input id="received_date" type="text" class="form-control input-sm datepicker">
		                    </div>
		                    @if($document->d_request_type == 2)
			                    <div class="form-group">
			                        <label class="col-sm-2 control-label">Disposal Date</label>
		                            <input id="disposal_date" type="text" class="form-control input-sm datepicker">
			                    </div>
		                    @endif
			            </div>
		            @endif
		            <div class="clearfix"></div>
		            <button type="submit" class="btn btn-danger btn-sm btn-save">Submit</button>
		        </form>
		    </div>
		</div>
	</div>
@endsection
@section("scripts")
	<script type="text/javascript">
		$('.datepicker').datepicker({
	        dateFormat : 'yy-mm-dd'
	    });
		$(".btn-save").on("click",function(e) {
			e.preventDefault();
			axios.post(base_url+'/ess/document/send', {
				"request_no" : '{{ $document->d_request_no }}',
				"site" : $("#site").val(),
				"chrd_remarks" : $("#chrd_remarks").val(),
				"received_date" : $("#received_date").val(),
				"disposal_date" : $("#disposal_date").val(),
			})
	        .then(function (response) {
				if(response.data.success) {
					alert(response.data.message)
					setTimeout(function(){ 
						location.reload(); 
					}, 1000);
				}
			})
			.catch(function (error) {
			});
		})
	</script>
@endsection
