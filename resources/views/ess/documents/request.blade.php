@extends('layouts.app')

@section('content')
	<div class="panel panel-default panel-as">
	    <div class="panel-heading">Facilities Management</div>
        <div class="panel-body">
		    <div class="div-add-furniture">
		        <form id="request-document">
		            <div class="col-md-6 col-md-offset-3">
		                <div class="form-horizontal">
		                    <div class="form-group">
		                        <label class="col-sm-3 control-label">Request Type</label>
		                        <div class="col-sm-9">
		                            <select type="text" id="doc_request_type" name="doc_request_type" class="form-control input-sm">
		                            	<option value="1">Storage</option>
		                            	<option value="2">Disposal</option>
		                            </select>
		                        </div>
		                    </div>
		                    <div class="form-group">
		                        <label for="inputPassword3" class="col-sm-3 control-label">description</label>
		                        <div class="col-sm-9">
		                            <textarea type="text" id="doc_description" name="doc_description" class="form-control input-sm"></textarea>
		                        </div>
		                    </div>
		                </div>
		                <div class="col-md-6 text-center col-md-offset-3">
		                	<button type="submit" class="btn btn-danger btn-sm btn-save">SAVE</button>
				            <button type="button" style="display:none" class="btn btn-danger btn-sm btn-update">SAVE</button>
				            <button id="reset-facility" class="btn btn-default btn-sm">RESET</button>
		                </div>
		            </div>
		        </form>
		    </div>
		</div>
	</div>
@endsection
@section("scripts")
	<script type="text/javascript">
		$(".btn-save").on("click",function(e) {
			e.preventDefault();
			var form_data = $("#request-document").serialize();
			axios.post(base_url+'/ess/document/request', form_data)
	        .then(function (response) {
				if(response.data.success) {
					alert(response.data.message)
					setTimeout(function(){ 
						location.reload(); 
					}, 1000);
				}
			})
			.catch(function (error) {
			});
		})
	</script>
@endsection
