<table border="1">
    <thead>
        <tr>
            <th colspan="3" style="text-align: left;">EVENT NAME : {{ $event->ev_name }}</th>
        </tr>
        <tr>
            <th colspan="3" style="text-align: left;">EVENT DATE : {{ $event->ev_date }} </th>
        </tr>
        <tr>
            <th style="text-align: left;" >NAME : </th>
            <th style="text-align: left;" >DEPARTMENT NAME : </th>
            <th style="text-align: left;" >PLATE NO : </th>
        </tr>
    </thead>
    <tbody>
        <tr>
            <td colspan="3"><strong>ATTENDING</strong></td>
        </tr>
        @foreach($event->attendees as $attendee)
            @if($attendee->ea_is_going == 1)
                <tr>
                    <td>{{ $attendee->employee->fullname }}</td>
                    <td>{{ $attendee->employee->department->dept_name }}</td>
                    <td>{{ $attendee->ea_plate_number }}</td>
                </tr>
            @endif
        @endforeach
        <tr>
            <td><strong>COUNT</strong></td>
            <td>{{ $going_c }}</td>
            <td></td>
        </tr>
        <tr>
            <td colspan="3"><strong>NOT ATTENDING</strong></td>
        </tr>
        @foreach($event->attendees as $attendee)
            @if($attendee->ea_is_going == 0)
                <tr>
                    <td>{{ $attendee->employee->fullname }}</td>
                    <td>{{ $attendee->employee->department->dept_name }}</td>
                    <td>{{ $attendee->ea_plate_number }}</td>
                </tr>
            @endif
        @endforeach
        <tr>
            <td><strong>COUNT</strong></td>
            <td>{{ $not_going_c }}</td>
            <td></td>
        </tr>
    </tbody>
</table>