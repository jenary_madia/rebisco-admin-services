<table class="table table-bordered" style="width: 100%">
    <thead class="table-header" style="text-align: left;">
        <tr>
            <th></th>
            <th>NAME</th>
            <th>DEPARTMENT</th>
            <th>PARTICULARS</th>
            <th>RECIPIENT</th>
            <th>DEPARTMENT</th>
            <th>RECEIVED</th>
        </tr>
        <tr>
            <th></th>
            <th colspan="2">DATE : {{ date('Y-m-d') }}</th>
        </tr>
    </thead>
    <tbody id="body_list_report">
        <?php $count = 1 ?>
        <?php $receivers = [] ?>
        @foreach($report_logs as $log)
            <?php $log->receiver_fullname ? array_push($receivers,$log->receiver_fullname) : "" ?>
            <tr>
                <td>{{ $count }}</td>
                <td>
                    {{ $log->sender_fullname }}
                </td>
                <td>
                    {{ $log->sender_dept }}
                </td>
                <td>
                    {{ $log->ls_particulars }}
                </td>
                <td>
                    {{ $log->recipient_fullname }}
                </td>
                <td>
                    {{ $log->recipient_dept }}
                </td>
                <td>
                    {{ $log->receiver_fullname }}
                </td>
            </tr>
            <?php $count++ ?>
        @endforeach
    </tbody>
</table>
<br>
RECEIVED BY:
@foreach($receivers as $receiver)
    <ul>
        <li>{{ $receiver }}</li>
    </ul>
@endforeach