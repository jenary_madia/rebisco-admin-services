@extends('layouts.app')

@section('content')
    <div class="col-md-12 text-center">
        <p style="font-size: 45px;">Hi <span style="text-transform: capitalize;">{{ Auth::user()->firstname }}</span>!</p>
        <p style="font-size:15px;">How can we help you today?</p>
    </div>
    <div class="col-md-8">
        <img src="http://via.placeholder.com/550x250" style="width: 100%">
    </div>
    <div class="clearfix"></div>
    <br>
    <br>
    <div class="col-md-4">
        <div class="col-mdpanel panel-default panel-as">
            <div class="panel-heading">Documents for Storage/Disposal</div>
            <div class="panel-body">
                 @foreach($documents as $document)
                    <div>
                        <a style="color:red" class="text-capitalize" href="{{ url('/ess/document',$document->d_id) }}">{{ $document->d_request_no }}</a>
                        <br>
                    </div>
                    <br>
                @endforeach
            </div>
        </div>
    </div>
    <div class="col-md-4">
        <div class="col-mdpanel panel-default panel-as" style="border: 1px solid #f3f3f3">
            <div class="panel-heading">Company Events</div>
            <div class="panel-body">
                @foreach($events as $event)
                    <div>
                        <a class="confirm_attendance" data-id="{{ $event->ev_id }}" style="color:red" class="text-capitalize" href="">{{ $event->ev_name }}</a>
                        <br>
                        <span>{{ strlen($event->ev_description) > 200 ? substr($event->ev_description, 0, 200).'....' : $event->ev_description }}</span>
                    </div>
                    <br>
                @endforeach
            </div>
        </div>
    </div>
    <div class="clearfix"></div>
    <br>
    <br>
    <!-- Modal -->
    <div class="modal fade" id="attendEvent" tabindex="-1" role="dialog" aria-labelledby="attendEventLabel">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title" id="attendEventLabel">Are you attending?</h4>
                </div>
                <div class="modal-body text-center">
                    <button type="button" class="btn btn-default" id="btn-not-attending">No</button>
                    <button type="button" class="btn btn-primary" id="btn-attending">Yes</button>
                </div>
            </div>
        </div>
    </div>

@endsection
@section("scripts")
    <script type="text/javascript">
        var event_id;
        $(".confirm_attendance").on('click',function(e) {
            e.preventDefault();
            event_id = $(this).data("id");
            $("#attendEvent").modal().show();
        });

        $("#btn-not-attending").on("click",function() {
            submitAttendance({
                ea_is_going : 0
            });
        });

        $("#btn-attending").on("click",function() {
            $("#attendEvent .modal-body").empty();
            $("#attendEvent .modal-body").append('<button type="button" style="margin-right: 4px;" class="btn btn-default" id="btn-no-car">No</button><button type="button" class="btn btn-primary" id="btn-has-car">Yes</button>');
            $("#attendEvent .modal-title").html("Are you bringing a car?");
        });

        $("body").on('click',"#btn-no-car",function() {
            submitAttendance({
                ea_is_going : 1,
                ea_has_car : 0,
            });
        });

        $("body").on('click',"#btn-has-car",function() {
            $("#attendEvent .modal-body").empty();
            $("#attendEvent .modal-body").append('<input type="text" id="ea_plate_number" class="input-sm" style="width: 150px; margin-right: 4px;" /><button type="button" class="btn btn-primary" id="btn-attend-confirm">Done</button>');
        });

        $("body").on('click',"#btn-attend-confirm",function() {
            var plate_number = $("#ea_plate_number").val();
            if(plate_number) {
                submitAttendance({
                    ea_is_going : 1,
                    ea_has_car : 1,
                    ea_plate_number : plate_number
                });
            }else{
                alert("Plate number is required");
            }
            
        });

        function submitAttendance(params) {
            axios.post(base_url+'/event/'+event_id+'/confirm-attendance',params)
            .then(function (response) {
                alert(response.data.message);
                setTimeout(function(){
                    location.reload();
                }, 1000);
            })
            .catch(function (error) {
            });
        }
    </script>
@endsection
