@extends('layouts.app')

@section('content')
    <div class="panel panel-default panel-as">
        <div class="panel-heading">Company Events</div>
        <div class="sub-panel-heading">
        	<ul class="list-inline text-center">
        		<li><a href="#events" aria-controls="events" role="tab" data-toggle="tab">Events</a></li>
        		<li><a href="#create-event" aria-controls="create-event" role="tab" data-toggle="tab">Create event</a></li>
	        </ul>
	    </div>
        
        <div class="tab-content">
            <div role="tabpanel" class="tab-pane fade" id="events">
                @include("events.tab-list")
            </div>
            <div role="tabpanel" class="tab-pane active" id="create-event">
                @include("events.tab-create")
            </div>
        </div>
    </div>

    <div>
    </div>

@endsection
@section('scripts')
    <script  src="{{ asset('js/events.js') }}"></script>
@endsection
