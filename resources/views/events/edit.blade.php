@extends('layouts.app')

@section('content')
    <div class="panel panel-default panel-as">
        <div class="panel-heading">Company Events</div>
        <div class="panel-body">
            <form id="event-edit">
                <div class="col-md-12">
                    <h3>Edit Event</h3>
                    <div class="form-inline event-info-one">
                        <div class="form-group">
                            <label>Event Name</label>
                            <input type="text" id="ev_name" class="form-control input-sm" value="{{ $event->ev_name }}">
                        </div>
                        <div class="form-group">
                            <label>Event Date</label>
                            <input type="text" id="ev_date" class="datepicker form-control input-sm" value="{{ $event->ev_date }}">
                        </div>
                    </div>
                </div>
                <div class="clearfix"></div>
                <br>
                <div class="col-md-12">
                    <div class="form-inline add-expense">
                        <div class="form-group">
                            <select id="ed_category" class="form-control input-sm">
                                <option value="venue">Venue</option>
                                <option value="food_beverages">Food and Beverages</option>
                                <option value="prizes">Prizes</option>
                                <option value="performers">Performers</option>
                                <option value="logistics">Logistics</option>
                            </select>
                        </div>
                        <div class="form-group">
                            <input type="text" class="form-control input-sm" id="ed_supplier" placeholder="Supplier Name">
                        </div>
                        <div class="form-group">
                            <input type="text" class="form-control input-sm numeric" id="ed_budget" placeholder="Budget">
                        </div>
                        <button class="btn-success btn btn-sm" id="add-event-details">ADD</button>
                    </div>
                    <div class="div-totals div-total-first">
                        <label>PHP</label>
                        <label id="total_budget">{{ $event->total->expenses }}</label>
                    </div>
                    <div class="div-totals">  
                        <label>PHP</label>
                        <label id="total_actual">{{ $event->total->actual }}</label>
                    </div>
                </div>
                <div class="clearfix"></div>
                <br>
                <div class="col-md-12">
                    <table class="table table-bordered">
                        <thead style="border : none;">
                            <tr>
                                <th style="border : none;" colspan="2">
                                    <label class="pull-left table-title">
                                        <small>Expenses</small><br>
                                        Venue
                                    </label>
                                </th>
                                <th style="border : none;" colspan="2">
                                    <label class="pull-right" id="total-venue"> 0</label><label class="pull-right" style="padding-right: 5px">PHP</label>
                                </th>
                            </tr>
                        </thead>
                        <thead class="table-header">
                            <tr>
                                <th>SUPPLIER</th>
                                <th>BUDGET</th>
                                <th>ACTUAL</th>
                                <th>ACTIONS</th>
                            </tr>
                        </thead>
                        <tbody class="tbl-body-venue">
                            @foreach($event->eventDetails as $details)
                                @if($details->ed_category == "venue")
                                    <tr data-category="venue">
                                        <td>
                                            <input data-name='ed_supplier' value="{{ $details->ed_supplier }}" readonly type='text' class='input_list form-control input-sm' style='width:100px'>
                                        </td>
                                        <td>
                                            <input data-name='ed_budget' value="{{ $details->ed_budget }}" readonly type='text' class='input_list form-control input-sm input-budget' style='width:100px'>
                                        </td>
                                        <td>
                                            <input  data-name='ed_actual' value="{{ (int) $details->ed_actual }}" readonly type='text' data-category='' class='form-control input-sm actual numeric' style='width:50px' maxlength='2'>
                                        </td>
                                        <td class="text-center">
                                            <button class='delete-icon container-icon'>
                                                <span class='glyphicon glyphicon-remove'></span>
                                            </button>
                                            <button class='edit-icon container-icon' type='button'>
                                                <span class='glyphicon glyphicon-pencil'></span>
                                            </button>
                                        </td>
                                    </tr>
                                @endif
                            @endforeach
                        </tbody>
                    </table>
                    <br>
                    <table class="table table-bordered">
                        <thead style="border : none;">
                            <tr>
                                <th style="border : none;" colspan="2">
                                    <label class="pull-left table-title">
                                        <small>Expenses</small><br>
                                        Food and Beverages
                                    </label>
                                </th>
                                <th style="border : none;" colspan="2">
                                    <label class="pull-right" id="total-food_beverages"> 0</label><label class="pull-right" style="padding-right: 5px">PHP</label>
                                </th>
                            </tr>
                        </thead>
                        <thead class="table-header">
                            <tr>
                                <th>SUPPLIER</th>
                                <th>BUDGET</th>
                                <th>ACTUAL</th>
                                <th>ACTIONS</th>
                            </tr>
                        </thead>
                        <tbody class="tbl-body-food_beverages">
                            @foreach($event->eventDetails as $details)
                                @if($details->ed_category == "food_beverages")
                                    <tr data-category="food_beverages">
                                        <td>
                                            <input data-name='ed_supplier' value="{{ $details->ed_supplier }}" readonly type='text' class='input_list form-control input-sm' style='width:100px'>
                                        </td>
                                        <td>
                                            <input data-name='ed_budget' value="{{ $details->ed_budget }}" readonly type='text' class='input_list form-control input-sm input-budget' style='width:100px'>
                                        </td>
                                        <td>
                                            <input  data-name='ed_actual' value="{{ (int) $details->ed_actual }}" readonly type='text' data-category='' class='form-control input-sm actual numeric' style='width:50px' maxlength='2'>
                                        </td>
                                        <td class="text-center">
                                            <button class='delete-icon container-icon'>
                                                <span class='glyphicon glyphicon-remove'></span>
                                            </button>
                                            <button class='edit-icon container-icon' type='button'>
                                                <span class='glyphicon glyphicon-pencil'></span>
                                            </button>
                                        </td>
                                    </tr>
                                @endif
                            @endforeach
                        </tbody>
                    </table>
                     <br>
                    
                    <table class="table table-bordered">
                        <thead style="border : none;">
                            <tr>
                                <th style="border : none;" colspan="2">
                                    <label class="pull-left table-title">
                                        <small>Expenses</small><br>
                                        Prizes
                                    </label>
                                </th>
                                <th style="border : none;" colspan="2">
                                    <label class="pull-right" id="total-prizes"> 0</label><label class="pull-right" style="padding-right: 5px">PHP</label>
                                </th>
                            </tr>
                        </thead>
                        <thead class="table-header">
                            <tr>
                                <th>SUPPLIER</th>
                                <th>BUDGET</th>
                                <th>ACTUAL</th>
                                <th>ACTIONS</th>
                            </tr>
                        </thead>
                        <tbody class="tbl-body-prizes">
                            @foreach($event->eventDetails as $details)
                                @if($details->ed_category == "prizes")
                                    <tr data-category="prizes">
                                        <td>
                                            <input data-name='ed_supplier' value="{{ $details->ed_supplier }}" readonly type='text' class='input_list form-control input-sm' style='width:100px'>
                                        </td>
                                        <td>
                                            <input data-name='ed_budget' value="{{ $details->ed_budget }}" readonly type='text' class='input_list form-control input-sm input-budget' style='width:100px'>
                                        </td>
                                        <td>
                                            <input  data-name='ed_actual' value="{{ (int) $details->ed_actual }}" readonly type='text' data-category='' class='form-control input-sm actual numeric' style='width:50px' maxlength='2'>
                                        </td>
                                        <td class="text-center">
                                            <button class='delete-icon container-icon'>
                                                <span class='glyphicon glyphicon-remove'></span>
                                            </button>
                                            <button class='edit-icon container-icon' type='button'>
                                                <span class='glyphicon glyphicon-pencil'></span>
                                            </button>
                                        </td>
                                    </tr>
                                @endif
                            @endforeach
                        </tbody>
                    </table>
                     <br>
                    
                    <table class="table table-bordered">
                        <thead style="border : none;">
                            <tr>
                                <th style="border : none;" colspan="2">
                                    <label class="pull-left table-title">
                                        <small>Expenses</small><br>
                                        Performers
                                    </label>
                                </th>
                                <th style="border : none;" colspan="2">
                                    <label class="pull-right" id="total-performers"> 0</label><label class="pull-right" style="padding-right: 5px">PHP</label>
                                </th>
                            </tr>
                        </thead>
                        <thead class="table-header">
                            <tr>
                                <th>SUPPLIER</th>
                                <th>BUDGET</th>
                                <th>ACTUAL</th>
                                <th>ACTIONS</th>
                            </tr>
                        </thead>
                        <tbody class="tbl-body-performers">
                            @foreach($event->eventDetails as $details)
                                @if($details->ed_category == "performers")
                                    <tr data-category="performers">
                                        <td>
                                            <input data-name='ed_supplier' value="{{ $details->ed_supplier }}" readonly type='text' class='input_list form-control input-sm' style='width:100px'>
                                        </td>
                                        <td>
                                            <input data-name='ed_budget' value="{{ $details->ed_budget }}" readonly type='text' class='input_list form-control input-sm input-budget' style='width:100px'>
                                        </td>
                                        <td>
                                            <input  data-name='ed_actual' value="{{ (int) $details->ed_actual }}" readonly type='text' data-category='' class='form-control input-sm actual numeric' style='width:50px' maxlength='2'>
                                        </td>
                                        <td class="text-center">
                                            <button class='delete-icon container-icon'>
                                                <span class='glyphicon glyphicon-remove'></span>
                                            </button>
                                            <button class='edit-icon container-icon' type='button'>
                                                <span class='glyphicon glyphicon-pencil'></span>
                                            </button>
                                        </td>
                                    </tr>
                                @endif
                            @endforeach
                        </tbody>
                    </table>
                     <br>
                    
                    <table class="table table-bordered">
                        <thead style="border : none;">
                            <tr>
                                <th style="border : none;" colspan="2">
                                    <label class="pull-left table-title">
                                        <small>Expenses</small><br>
                                        Logistics
                                    </label>
                                </th>
                                <th style="border : none;" colspan="2">
                                    <label class="pull-right" id="total-logistics"> 0</label><label class="pull-right" style="padding-right: 5px">PHP</label>
                                </th>
                            </tr>
                        </thead>
                        <thead class="table-header">
                            <tr>
                                <th>SUPPLIER</th>
                                <th>BUDGET</th>
                                <th>ACTUAL</th>
                                <th>ACTIONS</th>
                            </tr>
                        </thead>
                        <tbody class="tbl-body-logistics">
                            @foreach($event->eventDetails as $details)
                                @if($details->ed_category == "logistics")
                                    <tr data-category="logistics">
                                        <td>
                                            <input data-name='ed_supplier' value="{{ $details->ed_supplier }}" readonly type='text' class='input_list form-control input-sm' style='width:100px'>
                                        </td>
                                        <td>
                                            <input data-name='ed_budget' value="{{ $details->ed_budget }}" readonly type='text' class='input_list form-control input-sm input-budget' style='width:100px'>
                                        </td>
                                        <td>
                                            <input  data-name='ed_actual' value="{{ (int) $details->ed_actual }}" readonly type='text' data-category='' class='form-control input-sm actual numeric' style='width:50px' maxlength='2'>
                                        </td>
                                        <td class="text-center">
                                            <button class='delete-icon container-icon'>
                                                <span class='glyphicon glyphicon-remove'></span>
                                            </button>
                                            <button class='edit-icon container-icon' type='button'>
                                                <span class='glyphicon glyphicon-pencil'></span>
                                            </button>
                                        </td>
                                    </tr>
                                @endif
                            @endforeach
                        </tbody>
                    </table>

                    @if($has_others)
                        <table class="tbl-event-details table table-bordered tbl-others">
                    @else
                        <table class="tbl-event-details table table-bordered tbl-others" style="display: none;">
                    @endif
                        <thead style="border : none;">
                            <tr>
                                <th style="border : none;" colspan="2">
                                    <label class="pull-left table-title">
                                        <small>Expenses</small><br>
                                        Others
                                    </label>
                                </th>
                                <th style="border : none;" colspan="2">
                                    <label class="pull-right" id="total-logistics"> 0</label><label class="pull-right" style="padding-right: 5px">PHP</label>
                                </th>
                            </tr>
                        </thead>
                        <thead class="table-header">
                            <tr>
                                <th>SUPPLIER</th>
                                <th>BUDGET</th>
                                <th>ACTUAL</th>
                                <th>ACTIONS</th>
                            </tr>
                        </thead>
                        <tbody class="tbl-body-logistics">
                            @foreach($event->eventDetails as $details)
                                @if($details->ed_category == "others")
                                    <tr data-category="logistics">
                                        <td>
                                            <input data-name='ed_supplier' value="{{ $details->ed_supplier }}" readonly type='text' class='input_list form-control input-sm' style='width:100px'>
                                        </td>
                                        <td>
                                            <input data-name='ed_budget' value="{{ $details->ed_budget }}" readonly type='text' class='input_list form-control input-sm input-budget' style='width:100px'>
                                        </td>
                                        <td>
                                            <input  data-name='ed_actual' value="{{ (int) $details->ed_actual }}" readonly type='text' data-category='' class='form-control input-sm actual numeric' style='width:50px' maxlength='2'>
                                        </td>
                                        <td class="text-center">
                                            <button class='delete-icon container-icon'>
                                                <span class='glyphicon glyphicon-remove'></span>
                                            </button>
                                            <button class='edit-icon container-icon' type='button'>
                                                <span class='glyphicon glyphicon-pencil'></span>
                                            </button>
                                        </td>
                                    </tr>
                                @endif
                            @endforeach
                        </tbody>
                    </table>
                </div>
                <div class="clearfix"></div>
                <br>
                <hr class="thick-orange">
                <div class="div-after-tables">
                    <div class="col-md-4">
                        <span class="column-header">Attachments 
                            <div class="upload-btn-wrapper">
                                <button class="btn btn-danger btn-sm">Browse</button>
                                <input type="file" multiple id="browse-image"/>
                            </div>
                        </span>
                        <div>
                            <ul class="list-unstyled attachments">
                                @foreach($event->attachments as $attachment)
                                    @if($attachment->type == 0)
                                        <li>
                                            <input type="hidden" name="" value='{!! $attachment !!}' class='input-attachment'>
                                            <a href='{{ url("download",[$attachment->random_filename,$attachment->original_filename]) }}'>
                                                {{ $attachment->original_filename }}
                                            </a>
                                            <button class='btn btn-xs btn-danger'>DELETE</button>
                                        </li>
                                    @endif
                                @endforeach
                            </ul>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <span class="column-header">Invite Release</span>
                        <br>
                        @if($event->ev_invite_date <= date("Y-m-d") && $event->ev_invite_date) 
                        <pre>{{ "yeah"  }}</pre>
                            <input type="text" class="form-control input-sm" value="{{ $event->ev_invite_date }}" id="ev_invite_date" readonly style="width : 150px; margin-right: 10px;">
                            <div class="upload-btn-wrapper">
                                <button disabled class="btn btn-danger btn-sm">ADD INVITE</button>
                            </div>
                            <div>
                                <ul class="list-unstyled invite">
                                    @foreach($event->attachments as $attachment)
                                        @if($attachment->type == 1)
                                            <li>
                                                <input type="hidden" name="" value='{!! $attachment !!}' class='input-attachment'>
                                                <a href='{{ url("download",[$attachment->random_filename,$attachment->original_filename]) }}'>
                                                    {{ $attachment->original_filename }}
                                                </a>
                                            </li>
                                        @endif
                                    @endforeach
                                </ul>
                            </div>
                        @else
                            <input type="text" class="datepicker form-control input-sm"  value="{{ $event->ev_invite_date }}" id="ev_invite_date" style="width : 150px; margin-right: 10px;">
                            <div class="upload-btn-wrapper">
                                <button class="btn btn-danger btn-sm">ADD INVITE</button>
                                <input type="file" id="browse-invite" />
                            </div>
                            <div>
                                <ul class="list-unstyled invite">
                                    @foreach($event->attachments as $attachment)
                                        @if($attachment->type == 1)
                                            <li>
                                                <input type="hidden" name="" value='{!! $attachment !!}' class='input-attachment'>
                                                <a href='{{ url("download",[$attachment->random_filename,$attachment->original_filename]) }}'>
                                                    {{ $attachment->original_filename }}
                                                </a>
                                                <button class='btn btn-xs btn-danger'>DELETE</button>
                                            </li>
                                        @endif
                                    @endforeach
                                </ul>
                            </div>
                        @endif
                    </div>
                    <div class="col-md-4">
                        
                        @if($event->ev_invite_date <= date("Y-m-d")  && $event->ev_invite_date )
                            <span id="btn-summary" class="column-header">Summary of Attendees</span>
                        @else
                            <span class="column-header">Summary of Attendees</span>
                        @endif

                        <div class="form-group">
                            <label class="pull-left">Attending</label>
                            <input type="text" readonly value="{{ $going_c }}" class="pull-right form-control input-sm" style="width : 120px;">
                        </div>
                        <br>
                        <div class="form-group">
                            <label class="pull-left">Non - Attending</label>
                            <input type="text" readonly value="{{ $not_going_c }}" class="pull-right form-control input-sm" style="width : 120px;">
                        </div>
                    </div>
                </div>
                <div class="clearfix"></div>
                <br>
                <hr class="thick-orange">
                <div class="col-md-12">
                    <span class="column-header">Description</span><br>
                    <textarea id="ev_description" class="input-sm form-control">{{ $event->ev_description }}</textarea>
                </div>
                <div class="clearfix"></div>
                <br>
                <hr class="thick-orange">
                <div class="col-md-12">
                    <span class="column-header">Remarks</span><br>
                </div>
                <div class="div-remarks div-remarks-old">
                    @foreach($event->remarks as $remarks)
                        <div class="col-md-2 name">
                            {{ $remarks->emp_fullname }}
                        </div>
                        <div>
                            <div class="col-md-8 comment">
                                {{ $remarks->er_comment }}
                            </div>
                            <div class="col-md-2 date">
                                {{ Carbon\Carbon::parse($remarks->er_datetime)->format('Y/m/d') }}
                            </div>
                        </div>
                        <div class="clearfix"></div>
                        <br>
                    @endforeach
                </div>
                <div class="clearfix"></div>
                <br>    
                <div class="div-remarks div-new-comment">
                    <div class="col-md-2 name">
                        {{ Auth::user()->firstname.' '.Auth::user()->lastname }}
                    </div>
                    <div class="col-md-10">
                        <textarea class="form-control pull-left new-comment"></textarea>
                        <button class="pull-right btn btn-warning btn-send-comment">SEND</button>
                    </div>       
                </div>
                <div class="clearfix"></div>
                <br>
                <div class="col-md-12 text-center">
                    <button type="submit" class="btn btn-danger btn-sm">SAVE</button>
                    <a href="{{ route('events.index') }}" class="btn btn-danger btn-sm">BACK</a>
                </div>  
            </form>   
            <form method="POST" id="form-summary" action='{{ url("/event/$event->ev_id/download-attendees") }}'>
                <input type="hidden" name="_token" value="{{ csrf_token() }}">
            </form>
        </div>
    </div>

    <div>
    </div>
@endsection
@section('scripts')
    <script>
        $("#btn-summary").on("click",function() {
            $("#form-summary").submit();
        });
        window.ex_total_actual = '{!! $event->total_actual !!}';
        window.ev_id = '{!! $event->ev_id !!}';
        window.url_download = '{!! url("download") !!}'
        $(".btn-send-comment").on("click",function(e) {
            e.preventDefault();
            var new_comment = $(".new-comment").val();
            axios.post(base_url+'/event/'+ev_id+"/add-comment", {
                'comment' : new_comment,
            })
            .then(function (response) {
                var data = response.data.data;
                $(".div-remarks-old").append(`<div class="clearfix"></div>
                <br>
                <div>
                    <div class="col-md-2 name">`+$(".div-new-comment .name").html()+`</div>
                    <div class="col-md-8 added-comment">`+data.er_comment +`</div>
                    <div class="col-md-2 date">`+data.er_datetime+`</div>
                </div>`);
                 $(".new-comment").val("");
            })
            .catch(function (error) {
            });
        });
    </script>
    <script  src="{{ asset('js/events.js') }}"></script>
@endsection
