<div class="panel-body">
    <div class="col-md-12">
        <span class="pull-left page-title">Events</span>
    </div>
    <div class="col-md-12">
        <span class="sub-header">Filter Documents</span>
        <hr class="hr-no-margin">
        <br>
        <form class="form-inline">
            <div class="form-group">
                <label class="text-uppercase required" for="exampleInputName2">YEAR</label>
                <select type="text" class="form-control input-sm" style="width : 100px; margin-left : 10px; margin-right : 10px" id="exampleInputName2">
                    @foreach($years as $key => $value)
                        <option value="{{ $key }}" {{ $key == date('Y') ? "selected" : "" }}>{{ $value }}</option>
                    @endforeach
                </select>
            </div>
            <div class="form-group">
                <input style="width : 100px; margin-left : 10px; margin-right : 10px" type="email" class="form-control input-sm" placeholder="Search">
            </div>
            <!-- <button type="submit" class="btn btn-danger btn-sm pull-right">FILTER</button> -->
        </form>
        <br>
        <table id="list-events" class="table table-bordered">
            <thead class="table-header">
                <tr>
                    <th>EVENT NAME</th>
                    <th>DATE</th>
                    <th>TOTAL EXPENSES</th>
                </tr>
            </thead>
            <tbody class="text-center">
                @foreach($events as $event)
                    <tr class="event_row" data-id="{{ $event->ev_id }}">
                        <td>{{ $event->ev_name }}</td>
                        <td>{{ $event->ev_date }}</td>
                        <td>{{ $event->total ? $event->total->expenses : 0.00  }}</td>
                    </tr>
                @endforeach
            </tbody>
        </table>
    </div>
</div>