<div class="panel-body">
    <form id="event-create">
        <div class="col-md-12">
            <h3>Create Events</h3>
            <div class="form-inline event-info-one">
                <div class="form-group">
                    <label>Event Name</label>
                    <input type="text" id="ev_name" class="form-control input-sm">
                </div>
                <div class="form-group">
                    <label>Event Date</label>
                    <input type="text" id="ev_date" class="datepicker form-control input-sm">
                </div>
            </div>
        </div>
        <div class="clearfix"></div>
        <br>
        <div class="col-md-12">
            <div class="form-inline add-expense">
                <span class="title" align="center">ADD EXPENSE</span>
                <div class="form-group">
                    <select id="ed_category" class="form-control input-sm">
                        <option selected disabled></option>
                        <option value="venue">Venue</option>
                        <option value="food_beverages">Food and Beverages</option>
                        <option value="prizes">Prizes</option>
                        <option value="performers">Performers</option>
                        <option value="logistics">Logistics</option>
                        <option value="others">Others</option>
                    </select>
                </div>
                <div class="form-group">
                    <input type="text" class="form-control input-sm" maxlength="150" id="ed_supplier" placeholder="Supplier Name">
                </div>
                <div class="form-group">
                    <input type="text" class="form-control input-sm numeric" id="ed_budget" placeholder="Budget">
                </div>
                <button class="btn-success btn btn-sm" id="add-event-details">ADD</button>
            </div>
            <div class="div-totals div-total-first">
                <span class="title">TOTAL BUDGET</span>
                <label>PHP</label>
                <label id="total_budget"></label>
            </div>
            <div class="div-totals">  
                <span class="title">TOTAL ACTUAL</span>
                <label>PHP</label>
                <label id="total_actual"></label>
            </div>
        </div>
        <div class="clearfix"></div>
        <br>
        <div class="col-md-12">
            <table class="tbl-event-details table table-bordered">
                <thead style="border : none;">
                    <tr>
                        <th style="border : none;" colspan="2">
                            <label class="pull-left table-title">
                                <small>Expenses</small><br>
                                Venue
                            </label>
                        </th>
                        <th style="border : none;" colspan="2">
                            <label class="pull-right" id="total-venue"> 0</label><label class="pull-right" style="padding-right: 5px">PHP</label>
                        </th>
                    </tr>
                </thead>
                <thead class="table-header">
                    <tr>
                        <th>SUPPLIER</th>
                        <th>BUDGET</th>
                        <th>ACTUAL</th>
                        <th>ACTIONS</th>
                    </tr>
                </thead>
                <tbody class="tbl-body-venue">
                </tbody>
            </table>
            <br>
            
            <table class="tbl-event-details table table-bordered">
                <thead style="border : none;">
                    <tr>
                        <th style="border : none;" colspan="2">
                            <label class="pull-left table-title">
                                <small>Expenses</small><br>
                                Food and Beverages
                            </label>
                        </th>
                        <th style="border : none;" colspan="2">
                            <label class="pull-right" id="total-food_beverages"> 0</label><label class="pull-right" style="padding-right: 5px">PHP</label>
                        </th>
                    </tr>
                </thead>
                <thead class="table-header">
                    <tr>
                        <th>SUPPLIER</th>
                        <th>BUDGET</th>
                        <th>ACTUAL</th>
                        <th>ACTIONS</th>
                    </tr>
                </thead>
                <tbody class="tbl-body-food_beverages">
                </tbody>
            </table>
             <br>
            
            <table class="tbl-event-details table table-bordered">
                <thead style="border : none;">
                    <tr>
                        <th style="border : none;" colspan="2">
                            <label class="pull-left table-title">
                                <small>Expenses</small><br>
                                Prizes
                            </label>
                        </th>
                        <th style="border : none;" colspan="2">
                            <label class="pull-right" id="total-prizes"> 0</label><label class="pull-right" style="padding-right: 5px">PHP</label>
                        </th>
                    </tr>
                </thead>
                <thead class="table-header">
                    <tr>
                        <th>SUPPLIER</th>
                        <th>BUDGET</th>
                        <th>ACTUAL</th>
                        <th>ACTIONS</th>
                    </tr>
                </thead>
                <tbody class="tbl-body-prizes">
                </tbody>
            </table>
             <br>
            
            <table class="tbl-event-details table table-bordered">
                <thead style="border : none;">
                    <tr>
                        <th style="border : none;" colspan="2">
                            <label class="pull-left table-title">
                                <small>Expenses</small><br>
                                Performers
                            </label>
                        </th>
                        <th style="border : none;" colspan="2">
                            <label class="pull-right" id="total-performers"> 0</label><label class="pull-right" style="padding-right: 5px">PHP</label>
                        </th>
                    </tr>
                </thead>
                <thead class="table-header">
                    <tr>
                        <th>SUPPLIER</th>
                        <th>BUDGET</th>
                        <th>ACTUAL</th>
                        <th>ACTIONS</th>
                    </tr>
                </thead>
                <tbody class="tbl-body-performers">
                </tbody>
            </table>
             <br>
            
            <table class="tbl-event-details table table-bordered">
                <thead style="border : none;">
                    <tr>
                        <th style="border : none;" colspan="2">
                            <label class="pull-left table-title">
                                <small>Expenses</small><br>
                                Logistics
                            </label>
                        </th>
                        <th style="border : none;" colspan="2">
                            <label class="pull-right" id="total-logistics"> 0</label><label class="pull-right" style="padding-right: 5px">PHP</label>
                        </th>
                    </tr>
                </thead>
                <thead class="table-header">
                    <tr>
                        <th>SUPPLIER</th>
                        <th>BUDGET</th>
                        <th>ACTUAL</th>
                        <th>ACTIONS</th>
                    </tr>
                </thead>
                <tbody class="tbl-body-logistics">
                </tbody>
            </table>

            <table class="tbl-event-details table table-bordered tbl-others" style="display: none;">
                <thead style="border : none;">
                    <tr>
                        <th style="border : none;" colspan="2">
                            <label class="pull-left table-title">
                                <small>Expenses</small><br>
                                Others
                            </label>
                        </th>
                        <th style="border : none;" colspan="2">
                            <label class="pull-right" id="total-logistics"> 0</label><label class="pull-right" style="padding-right: 5px">PHP</label>
                        </th>
                    </tr>
                </thead>
                <thead class="table-header">
                    <tr>
                        <th>SUPPLIER</th>
                        <th>BUDGET</th>
                        <th>ACTUAL</th>
                        <th>ACTIONS</th>
                    </tr>
                </thead>
                <tbody class="tbl-body-others">
                </tbody>
            </table>
        </div>
        <div class="clearfix"></div>
        <br>
        <hr class="thick-orange">
        <div class="div-after-tables">
            <div class="col-md-4">
                <span class="column-header">Attachments 
                    <div class="upload-btn-wrapper">
                        <button class="btn btn-danger btn-sm">ADD INVITE</button>
                        <input type="file" multiple id="browse-image"/>
                    </div>
                </span>
                <div>
                    <ul class="list-unstyled attachments">
                    </ul>
                </div>
            </div>
            <div class="col-md-4">
                <span class="column-header">Invite Release</span>
                <br>
                <input type="text" id="ev_invite_date" class="datepicker pull-left form-control input-sm" style="width : 150px; margin-right: 10px;">
                <div class="upload-btn-wrapper">
                    <button class="btn btn-danger btn-sm">ADD INVITE</button>
                    <input type="file" id="browse-invite" />
                </div>
                <div>
                    <ul class="list-unstyled invite">
                    </ul>
                </div>
            </div>
            <div class="col-md-4">
                <span class="column-header">Summary of Attendees</span><br>
                <div class="form-group">
                    <label class="pull-left">Attending</label>
                    <input type="text" class="pull-right form-control input-sm" style="width : 120px;">
                </div>
                <br>
                <div class="form-group">
                    <label class="pull-left">Non - Attending</label>
                    <input type="text" class="pull-right form-control input-sm" style="width : 120px;">
                </div>
            </div>
        </div>
        <div class="clearfix"></div>
        <br>
        <hr class="thick-orange">
        <div class="col-md-12">
            <span class="column-header">Description</span><br>
            <textarea id="ev_description" class="input-sm form-control"></textarea>
        </div>
        <div class="clearfix"></div>
        <br>
        <!-- <div class="col-md-12">
            <span class="column-header">Remarks</span><br>
        </div>
        <div class="div-remarks">
            <div class="col-md-2 name">
                juan dela cruz
            </div>
            <div>
                <div class="col-md-8 comment">
                    Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
                    tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,
                    quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo
                    consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse
                    cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non
                    proident, sunt in culpa qui officia deserunt mollit anim id est laborum.
                </div>
                <div class="col-md-2 date">
                    11/12/2017
                    12:12 PM
                </div>
            </div>
        </div> -->
        
        <div class="col-md-12 text-center">
            <button type="submit" class="btn btn-danger btn-sm">SAVE</button>
        </div>  
    </form>   
</div>
