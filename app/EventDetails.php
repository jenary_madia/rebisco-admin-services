<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class EventDetails extends Model
{
    protected $table = "event_details";
    protected $guarded = [];
    public $timestamps = false;
}
