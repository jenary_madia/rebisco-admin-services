<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use DB;
use Auth;

class Documents extends Model
{
	protected $table = "documents";
	protected $primaryKey = "d_id";
    protected $guarded = [];
    public $timestamps = false;

    public static function generateRequestNo() {
    	$latest_request_no = Documents::max("d_request_no");
    	if ($latest_request_no)
		{
			$latest_year = explode("-",$latest_request_no)[0];
			if($latest_year == date("Y")) {
				return $latest_year.'-'.str_pad(explode("-",$latest_request_no)[1] + 1, 5, '0', STR_PAD_LEFT);
			}else{
				return date("Y").'-'.str_pad('00000' + 1, 5, '0', STR_PAD_LEFT);
			}
		}else{
			return date("Y").'-'.str_pad('00000' + 1, 5, '0', STR_PAD_LEFT);

		}
    }

    public static function forApproval($status) {
        return Documents::where([
            'd_status' => $status,
        	'd_current_id' => Auth::user()->id
        ])->with("owner")->get();
    }

    public function owner() {
    	return $this->hasOne("App\Employees","id","d_created_by")->leftJoin("hrms_departments","employees.departmentid2","hrms_departments.dept_code")->selectRaw("firstname+' '+middlename+'. '+lastname as fullname,employees.id,hrms_departments.dept_name");
    }
}
