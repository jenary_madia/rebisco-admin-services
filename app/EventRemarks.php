<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class EventRemarks extends Model
{
    protected $table = "event_remarks";
    protected $guarded = [];
    public $timestamps = false;
}
