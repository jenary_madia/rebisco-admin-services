<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use DB;
use Auth;

class Logsheets extends Model
{
    protected $table = "logsheets";
    protected $guarded = [];
    protected $primaryKey = "ls_id";
    public $timestamps = false;

    public static function incoming() {
    	return Self::join("employees as sender","logsheets.ls_sender_id","sender.id")
        ->join("employees as receiver","logsheets.ls_receiver_id","receiver.id")
        ->where("ls_created_by",'!=',Auth::user()->id)
        ->where("ls_is_received",null)
        ->select("logsheets.*",DB::raw("sender.firstname + ' ' + sender.middlename + ' ' + sender.lastname as sender_fullname,receiver.firstname + ' ' + receiver.middlename + ' ' + receiver.lastname as receiver_fullname,sender.departmentid2 as sender_dept,receiver.departmentid2 as receiver_dept"))
        ->get();

//   sender.firstname + ' ' + sender.middlename + ' ' + sender.lastname as sender_fullname

    }

    public static function outgoing() {
    	return Self::join("employees as sender","logsheets.ls_sender_id","sender.id")
        ->join("employees as receiver","logsheets.ls_receiver_id","receiver.id")
        ->where([
            "ls_created_by" => Auth::user()->id
        ])
        ->where("ls_is_received",null)
        ->select("logsheets.*",DB::raw("sender.firstname + ' ' + sender.middlename + ' ' + sender.lastname as sender_fullname,receiver.firstname + ' ' + receiver.middlename + ' ' + receiver.lastname as receiver_fullname,sender.departmentid2 as sender_dept,receiver.departmentid2 as receiver_dept"))
        ->get();
    }

    public static function transmital($where_clause = null) {
        $data = Self::join("employees as sender","logsheets.ls_sender_id","sender.id")
        ->join("employees as recipient","logsheets.ls_receiver_id","recipient.id")
        ->leftJoin("employees as receiver","logsheets.ls_received_by","receiver.id")
        ->where([
            "ls_created_by" => Auth::user()->id
        ]);

        if($where_clause) {
            $data->where($where_clause);
        }

        return $data->select("logsheets.*",DB::raw("sender.firstname + ' ' + sender.middlename + ' ' + sender.lastname as sender_fullname,recipient.firstname + ' ' + recipient.middlename + ' ' + recipient.lastname as recipient_fullname,sender.departmentid2 as sender_dept,recipient.departmentid2 as recipient_dept,receiver.firstname + ' ' + receiver.middlename + ' ' + receiver.lastname as receiver_fullname"))
        ->get();
    }

    public static function homeIncoming($limit = null) {
        $result = Logsheets::join("selectitems as origin","origin.item","logsheets.ls_origin")
        ->where("ls_created_by",'!=',Auth::user()->id)
        ->where("ls_is_received",null)
        ->selectRaw("logsheets.*,CONVERT(VARCHAR,logsheets.ls_time_sent,100) as ls_time_sent,CONVERT(VARCHAR,logsheets.ls_date_sent,100) as ls_date_sent,origin.text");
        if($limit)
            $result->limit($limit);

        return $result->get();
    }
}
