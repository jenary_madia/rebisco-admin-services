<?php

namespace App\Http\Requests;

use Illuminate\Http\Response;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Contracts\Validation\Validator;
use Illuminate\Http\Exceptions\HttpResponseException;

class EventRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            "ev_date" => "required|date_format:Y-m-d",
            "ev_description" => "required",
            "ev_name" => "required|unique:events,ev_name",
            "event_details.*.*" => "required"
        ];
    }

    public function messages()
    {
        return [
            "ev_date.required" => "Event date is required.",
            "ev_date.date_format" => "Event date is invalid.",
            "ev_description.required" => "Event description is required.",
            "ev_name.required" => "Event name is required.",
            "ev_name.unique" => "Event name is already taken.",
            "event_details.*.ed_budget.required" => "Budget is required.",
            "event_details.*.ed_category.required" => "Category is required.",
            "event_details.*.ed_supplier.required" => "Supplier is required.",
            "event_details.*.ed_actual.required" => "Actual is required.",
        ];
            
    }

    protected function failedValidation(Validator $validator)
    {
        $error_messages = [];
        foreach($validator->messages()->getMessages() as $field_name => $messages) {
            foreach($messages AS $message) {
                array_push($error_messages,$message);
            }
        }
        throw new HttpResponseException(response()->json([
            'success' => false,
            'message' => 'All fields are required',
            'errors' => $error_messages
        ], 200));
    }
}
