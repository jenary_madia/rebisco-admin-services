<?php

namespace App\Http\Requests;

use Illuminate\Http\Response;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Contracts\Validation\Validator;
use Illuminate\Http\Exceptions\HttpResponseException;

class OutgoingRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            "other_data.ls_origin" => "required",
            "other_data.ls_destination" => "required",
            "other_data.ls_date_sent" => 'required|date_format:"M d, Y"',
            "other_data.ls_time_sent" => 'required|date_format:"g:i A"',
        ];
    }

    public function messages()
    {
        return [
            "other_data.ls_date_sent.required" => "Date sent field is required.",
            "other_data.ls_time_sent.required" => "Time field is required.",
            "other_data.ls_destination.required" => "Destination field is required.",
            "other_data.ls_origin.required" => "Document origin field is required.",
            "other_data.ls_date_sent.date_format" => "Date sent field format is incorrect.",
            "other_data.ls_time_sent.date_format" => "Time field format is incorrect.",
        ];
            
    }

    // public function response(array $errors)
    // {
    //     return Response::create([
    //         "success" => false,
    //         "message" => "All fields are required",
    //         "errors" =>$errors,
    //     ],200);
    // }

    protected function failedValidation(Validator $validator)
    {
        $error_messages = [];
        foreach($validator->messages()->getMessages() as $field_name => $messages) {
            foreach($messages AS $message) {
                array_push($error_messages,$message);
            }
        }
        throw new HttpResponseException(response()->json([
            'success' => false,
            'message' => 'All fields are required',
            'errors' => $error_messages
        ], 200));
    }
}
