<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Http\Response;

class FacilityRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            "fa_name"=>"required", 
            "fa_type"=>"required", 
            "fa_acquisition_date"=>"required|date_format:Y-m-d", 
            "fa_retirement_date"=>"required|date_format:Y-m-d", 
            "fa_site"=>"required", 
            "fa_area"=>"required", 
            "fa_asset_number"=>"required"
        ];
    }

    public function messages()
    {
        return [
            "fa_name.required"=>"Furniture name is required", 
            "fa_type.required"=>"Furniture type is required", 
            "fa_acquisition_date.required"=>"Acquisition date is required", 
            "fa_retirement_date.required"=>"Retirement date is required",
            "fa_site.required"=>"Site is required", 
            "fa_area.required"=>"Area is required", 
            "fa_asset_number.required"=>"Asset number is required",
            "fa_retirement_date.date_format" => "Retirement date is invalid",
            "fa_acquisition_date.date_format" => "Acquisition date is invalid",
        ];
            
    }

    public function response(array $errors)
    {
        return Response::create([
            "success" => false,
            "message" => "All fields are required",
            "errors" =>$errors,
        ],200);
    }
}
