<?php

namespace App\Http\Controllers;

use App\Logsheets;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;


class AdminController extends Controller
{
    public function index()
    {
        $logsheets = Logsheets::homeIncoming(10);
        return view('admin.dashboard',compact("logsheets"));
    }

    public function viewAll($doc_type) {
        switch ($doc_type) {
            case 'incoming':
                return Logsheets::homeIncoming(10);
                break;
            
            case 'outgoing':
                // return Logsheets::homeIncoming(10);
                break;

        }
    }
}
