<?php

namespace App\Http\Controllers;

use App\AuditTrails;
use App\Employees;
use App\Logsheets;
use App\SelectItems;
use Auth;
use Carbon\Carbon;
use DB;
use Illuminate\Http\Request;
use App\Http\Requests\OutgoingRequest;
use PDF;

class DocumentLogsheetController extends Controller
{
    public $audit_trail;

    public function __construct(AuditTrails $auditTrail) {
        $this->audit_trail = $auditTrail;
    }

    public function index($type = null) {
    	$locations = SelectItems::where([
    		'module' => 'outgoing_document',
    		'modulesfield' => 'origin',
    	])->get();
        $outgoing_logs = Logsheets::outgoing();
        $incoming_logs = Logsheets::incoming();
        $report_logs = Logsheets::transmital();
    	$employees = Employees::where("active",1)->select("id",DB::raw("firstname+' '+middlename+' '+lastname as fullname"),"departmentid2")->get();
    	return view('document-logsheet.index',compact('locations','employees','outgoing_logs','incoming_logs','report_logs','type'));
    }

    public function store(OutgoingRequest $request) {
        $new_data = [];
        if($request->new_data) {
            $errors = [];
            $error = false;
            foreach ($request->new_data as $key) {
                if (array_filter((array) $key)) {
                    array_push($new_data, $key);
                }
            }
            foreach ($new_data as $key) {
                foreach ($key as $value) {
                    if(! $value) {
                        array_push($errors, 'Please check items with incomplete details.');
                        $error = true;
                    }
                    if($error) {
                        return Response([
                            'success' => false,
                            'errors' => $errors,
                            'message' => "Something went wrong.",
                        ],200);
                    }
                }
                if($key['ls_sender_id'] == $key['ls_receiver_id']) {
                    $error = true;
                    array_push($errors, 'Sender and Recipient should not be the same.');
                }
                if($error) {
                    return Response([
                        'success' => false,
                        'errors' => $errors,
                        'message' => "Something went wrong.",
                    ],200);
                }
            }
                
        }

         if($request->edited_data) {
            foreach ($request->edited_data as $key) {
                $errors = [];
                $error = false;
                if(! $key['ls_particulars']) {
                    $error = true;
                    array_push($errors, 'Please check items with incomplete details.');
                }
                if(! $key['ls_receiver_id']) {
                    $error = true;
                    array_push($errors, 'Please check items with incomplete details.');
                }
                if(! $key['ls_sender_id']) {
                    $error = true;
                    array_push($errors, 'Please check items with incomplete details.');
                }
                if($key['ls_sender_id'] == $key['ls_receiver_id']) {
                    $error = true;
                    array_push($errors, 'Sender and Recipient should not be the same.');
                }
                if($error) {
                    return Response([
                        'success' => false,
                        'errors' => $errors,
                        'message' => "Something went wrong.",
                    ],200);
                }
            }
        }
        DB::beginTransaction(); 
        try {
            $data = $request->all();
            $to_delete = [];
            $new_data = [];
            $to_insert = array_merge($data['edited_data'],$new_data);
            $a_fields = $data['other_data'];
            for ($i=0; $i < count($to_insert); $i++) { 
                if($to_insert[$i]){
                    if(isset($to_insert[$i]["ls_id"])) 
                        array_push($to_delete,$to_insert[$i]["ls_id"]);

                    array_push($new_data,[
                        "ls_created_by" => Auth::user()->id,
                        "ls_date_created" => Date("Y-m-d"),
                        "ls_sender_id" => $to_insert[$i]["ls_sender_id"], 
                        "ls_receiver_id" => $to_insert[$i]["ls_receiver_id"], 
                        "ls_particulars" => $to_insert[$i]["ls_particulars"], 
                        "ls_date_sent" => Carbon::parse($a_fields['ls_date_sent'])->format('Y-m-d'),
                        "ls_destination" => $a_fields["ls_destination"],
                        "ls_origin" => $a_fields["ls_origin"],
                        "ls_time_sent" => $a_fields["ls_time_sent"],
                    ]);
                }
                
            }
            Logsheets::whereIn("ls_id",$to_delete)->delete();
            Logsheets::insert($new_data);
            DB::commit();
            return Response([
                'success' => true,
                'message' => "Outgoing documents successfully saved",
            ],200);
        } catch (Exception $e) {
            DB::rollback();
            return Response([
                'success' => false,
                'message' => "Something went wrong",
            ],200);
        }
    }

    public function filter(Request $request,$type) {
        $all_request = $request->all();
        unset($all_request['_token']);
        foreach ($all_request as $key => $value) {
            if(! $all_request[$key]) {
                unset($all_request[$key]);
            }else{
                if ($key == "ls_date_from"){
                    $all_request["ls_date_sent"] = Carbon::parse($all_request[$key])->format('Y-m-d');
                    unset($all_request[$key]);
                }elseif($key == "ls_date_to"){
                    $date_to = $all_request[$key];
                    $date_from = $all_request['ls_date_sent'];
                    unset($all_request[$key]);
                    unset($all_request['ls_date_sent']);
                }elseif($key == "ls_date_sent"){
                    $all_request["ls_date_sent"] = Carbon::parse($all_request[$key])->format('Y-m-d');
                }

            }
            
        }
        // return $all_request;
        $logs = Logsheets::join("employees as sender","logsheets.ls_sender_id","sender.id")
        ->join("employees as receiver","logsheets.ls_receiver_id","receiver.id")
        ->leftJoin("employees as received_by","logsheets.ls_received_by","received_by.id");

        switch ($type) {
            case 'incoming':
                $all_request["ls_is_received"] = null;
                $logs->where($all_request)
                ->where('ls_created_by','!=',Auth::user()->id);
            break;

            case 'outgoing':
                $all_request["ls_is_received"] = null;
                $all_request["ls_created_by"] = Auth::user()->id;
                $logs->where($all_request);
            break;

            case 'report':
                $all_request["ls_created_by"] = Auth::user()->id;
                $logs->where($all_request);
                if(isset($date_to)) {
                    $logs->whereBetween('ls_date_sent',[Carbon::parse($date_from)->format('Y-m-d'), Carbon::parse($date_to)->format('Y-m-d')]);
                }
            break;
        }
        return $logs->select("logsheets.*",DB::raw("sender.firstname+' '+sender.middlename+' '+sender.lastname as sender_fullname,receiver.firstname+' '+receiver.middlename+' '+receiver.lastname as receiver_fullname,sender.departmentid2 as sender_dept,receiver.departmentid2 as receiver_dept,received_by.firstname+' '+received_by.middlename+' '+received_by.lastname as received_by"))
        ->get();

    }

    public function receive(Request $request) {
        DB::beginTransaction(); 
        try {
            $logsheet_ids = $request->all();
            Logsheets::whereIn('ls_id',$logsheet_ids)->update([
                'ls_is_received' => 1,
                'ls_received_by' => Auth::user()->id
            ]);
            DB::commit();
            return Response([
                'success' => true,
                'message' => "Incoming documents successfully received.",
            ],200);
        } catch (Exception $e) {
            DB::rollback();
            return Response([
                'success' => false,
                'message' => "Something went wrong",
            ],200);
        }
    }

    public function transmitalLog() {

        $transmital_logs = Logsheets::transmital();
        return view('document-logsheet.transmital',compact('transmital_logs'));
    }

    public function download(Request $request) {
        $all_request = [
            "ls_origin" => $request->ls_origin,
            "ls_destination" => $request->ls_destination,
            "ls_date_sent" => $request->ls_date_sent,
            "ls_time_sent" => $request->ls_time_sent,
        ];

        foreach ($all_request as $key => $value) {
            if(! $all_request[$key]) {
                unset($all_request[$key]);
            }
        }
        set_time_limit(0);
        $report_logs = Logsheets::transmital($all_request);
        // return view('pdf.document-report',compact('report_logs'));
        // $this->audit_trail->AU008($id,$tableName,$primaryKey,$module_id)
        $html = view('pdf.document-report',compact('report_logs'))->render();
        return PDF::load($html, 'A4', 'landscape')
            ->download();
    }

    public function delete(Request $request) {
        Logsheets::find($request->ls_id)->delete();
        return Response([
            'success' => true,
            'message' => "Outgoing documents successfully deleted",
        ],200);
    }
}
