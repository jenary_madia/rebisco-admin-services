<?php

namespace App\Http\Controllers;


use App\Attachments;
use App\AuditTrails;
use App\EventDetails;
use App\Events;
use App\Http\Requests\EventRequest;
use App\Mail\EventInvitation;
use DB;
use Illuminate\Auth\Access\Response;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Storage; 
use Illuminate\Support\Carbon;
use App\EventAttendees;
use PDF;
use App\EventRemarks;
use Auth;

class EventsController extends Controller
{
    public $audit_trail;

    public function __construct(AuditTrails $auditTrail) {
        $this->audit_trail = $auditTrail;
    }
    public function index() {
        $events = Events::with("total")
        ->get();

        $years = Events::selectRaw("distinct(YEAR(ev_date_created)) as year")->pluck("year","year");
        if(! array_key_exists(date('Y'), $years))
            $years[date('Y')] = date('Y'); 
    	return view('events.index',compact("events","years"));
    }

    public function store(EventRequest $request) {
    	DB::beginTransaction();
    	try {
            $attachments = $request->attachments;
    		$params = [];
            $event_params = [
                "ev_name" => $request->ev_name,
                "ev_date" => $request->ev_date,
                "ev_date_created" => date('Y-m-d'),
                "ev_created_by" => Auth::user()->id,
                "ev_description" => $request->ev_description,
                "ev_invite_date" => $request->ev_invite_date,
            ];
    		$event = Events::create($event_params);
            $this->audit_trail->AU001($event->ev_id,'events','ev_id',json_encode($event_params),1);
			$to_insert = [];
    		foreach ($request->event_details as $event_detail) {
    			if(count($event_detail) > 0) {
    				foreach ($event_detail as $data) {
    					array_push($to_insert,[
		    				"ed_ev_id" => $event->ev_id,
		    				"ed_category" => $data['ed_category'],
                            "ed_supplier" => $data['ed_supplier'],
                            "ed_budget" => $data['ed_budget'],
		    				"ed_actual" => $data['ed_actual'],
		    			]);
    				}
    			}
    		}
    		EventDetails::insert($to_insert);

            for ($i=0; $i < count($attachments); $i++) { 
                $attachments[$i]["module_id"] = $event->ev_id;
            }

            Attachments::insert($attachments);
    		DB::commit();
            return Response([
                'success' => true,
                'message' => "Event successfully added",
            ],200);
    	} catch (Exception $e) {
    		DB::rollback();
            return Response([
                'success' => false,
                'message' => "Something went wrong",
            ],200);
    	}
    }

    public function show($id) {
        $event = Events::where([
            'ev_id' => $id
        ])
        ->with('remarks','attendees','total','eventDetails','attachments')
        ->first();

        $has_others = false;

        if($event->eventDetails) {
            foreach ($event->eventDetails as $key) {
                if($key['ed_category'] == 'others')
                    $has_others = true;
            }
 
        }
        
        $going_c = 0;
        $not_going_c = 0;
        foreach ($event->attendees as $key) {
            if($key->ea_is_going == 1)
                $going_c++;
            else
                $not_going_c++;

        }
        $this->audit_trail->AU003($id,'events','ev_id',1);
        return view('events.edit',compact("event","going_c","not_going_c","has_others"));
    }

    public function update(Request $request,$id) {
        DB::beginTransaction();
        try {
            $attachments = $request->attachments;
            $event = Events::find($id);
            $old = json_encode($event);
            $params = [
                    "ev_name" => $request->ev_name,
                    "ev_date" => $request->ev_date,
                    "ev_description" => $request->ev_description,
                    "ev_invite_date" => $request->ev_invite_date,
                ];
            $event->update($params);
            $this->audit_trail->AU004($id,'events','ev_id',json_encode($params),$old,1);
            EventDetails::where('ed_ev_id',$id)->delete();
            $to_insert = [];
            foreach ($request->event_details as $event_detail) {
                if(count($event_detail) > 0) {
                    foreach ($event_detail as $data) {
                        array_push($to_insert,[
                            "ed_ev_id" => $event->ev_id,
                            "ed_category" => $data['ed_category'],
                            "ed_supplier" => $data['ed_supplier'],
                            "ed_budget" => $data['ed_budget'],
                            "ed_actual" => $data['ed_actual'],
                        ]);
                    }
                }
            }
            EventDetails::insert($to_insert);
            Attachments::where([
                'module_id' => $id,
                'module' => 'events',
            ])->delete();

            for ($i=0; $i < count($attachments); $i++) { 
                unset($attachments[$i]['a_id']);
                $attachments[$i]["module_id"] = $event->ev_id;
            }
            Attachments::insert($attachments);

            // $remarks = [];
            // foreach ($request->remarks as $key) {
            //     array_push($remarks,[
            //         'er_ev_id' => $id,
            //         'er_emp_id' => Auth::user()->id,
            //         'er_comment' => $key,
            //         'er_datetime' => Carbon::now()->toDateTimeString(),
            //     ]);
            // }

            EventRemarks::create([
                    'er_ev_id' => $id,
                    'er_emp_id' => Auth::user()->id,
                    'er_comment' => $request->comment,
                    'er_datetime' => Carbon::now()->toDateTimeString(),
                ]);
            DB::commit();
            return Response([
                'success' => true,
                'message' => "Event successfully updated",
            ],200);
        } catch (Exception $e) {
            DB::rollback();
            return Response([
                'success' => false,
                'message' => "Something went wrong",
            ],200);
        }
    }

    public function sendInvite() {
        $events = Events::where([
            'ev_invite_date' => date('Y-m-d')
        ])->with("invitation")->get();
        
        foreach ($events as $event) {
            $random_ext = $event['invitation']['random_filename'].'.'.$event['invitation']['original_extension'];
            Storage::copy('public/uploads/'.$event['invitation']['random_filename'],'public/uploads/'.$random_ext);
            Mail::to('jenary.madia@octaltech.net')
            ->send(new EventInvitation($event));
            Storage::delete(['public/uploads/'.$random_ext]);
        }
        
    }

    public function download(Request $request,$id) {

        $event = Events::with('attendees')->find($id);
        $going_c = 0;
        $not_going_c = 0;
        foreach ($event->attendees as $key) {
            if($key->ea_is_going == 1)
                $going_c++;
            else
                $not_going_c++;

        }
        set_time_limit(0);
        // return view('pdf.attendees-summary',compact('event','going_c','not_going_c'));
        // $this->audit_trail->AU008($id,$tableName,$primaryKey,$module_id)
        $html = view('pdf.attendees-summary',compact('event','going_c','not_going_c'))->render();
        return PDF::load($html, 'A4', 'landscape')
            ->download();
    }

    public function addComment(Request $request, $id) {
        DB::beginTransaction();
        try {
            $eventRemarks = EventRemarks::create([
                    'er_ev_id' => $id,
                    'er_emp_id' => Auth::user()->id,
                    'er_comment' => $request->comment ? $request->comment : "",
                    'er_datetime' => Carbon::now()->toDateTimeString(),
                ]);
            DB::commit();
            return Response([
                'success' => true,
                'data' => $eventRemarks,
                'message' => "Comment successfully added",
            ],200);
        } catch (Exception $e) {
            DB::rollback();
            return Response([
                'success' => false,
                'message' => "Something went wrong",
            ],200);
        }
    }

    public function confirmAttendance(Request $request, $event_id) {
        DB::beginTransaction();
        try {
            $to_insert = $request->all();
            $to_insert['ea_ev_id'] = $event_id;
            $to_insert['ea_employee_id'] = Auth::user()->id;
            DB::table("event_attendees")->insert($to_insert);
            DB::commit();
            return Response([
                'success' => true,
                'message' => "Attendance successfully confirmed",
            ],200);
        } catch (Exception $e) {
            DB::rollback();
            return Response([
                'success' => false,
                'message' => "Something went wrong",
            ],200);
        }
        
    }
}
