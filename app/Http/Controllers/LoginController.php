<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Employees;
use Illuminate\Foundation\Auth\AuthenticatesUsers;

class LoginController extends Controller
{	
	use AuthenticatesUsers;

	protected $username = 'username';

    public function login(Request $request) {
    	$result_arr = array();
		$username = $request->username;
		$password = md5($request->password);
		
		$emp = Employees::where("employees.username", "=", $username)->where("employees.password","=", $password)->where("employees.active", "<>", "0")->first([
			 "employees.id as employee_id"
			,"employees.employeeid"
			,"employees.new_employeeid"
			,"employees.firstname"
			,"employees.middlename"
			,"employees.lastname"
			,"employees.desig_level"
			,"employees.company"
			,"employees.designation"
			,"employees.superiorid"
			,"employees.username"
			,"employees.email"
			,"employees.departmentid2"
		 ]);

		if($emp) {
			dd($this->sendLoginResponse($request));
		}
    }


}
