<?php

namespace App\Http\Controllers\Ess;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Documents;
use DB;
use Auth;
use App\Employees;
use App\Receivers;

class DocumentController extends Controller
{
	public $employees;
	public function __construct(Employees $employees) {
		$this->employees = $employees;
	}

    public function store(Request $request) {
    	if(in_array(Auth::user()->desig_level,['employee','supervisor'])) {
    		$curr_emp = $this->employees->get_dept_head(Auth::user()->departmentid2);
    	}else{
    		$curr_emp = Receivers::where([
    			'module' => 'Documents',
    			'code'=> 'CHRD'
    		])->first()['employeeid'];
    	}

    	if(! $curr_emp) {
            return Response([
                'success' => false,
                'message' => "Receiver not declared.",
            ],200);
    	}

		DB::beginTransaction();
		try {
			$request_no = Documents::generateRequestNo();
	    	Documents::create([
	    		'd_created_by' => Auth::user()->id,
	    		'd_current_id' => $curr_emp,
	    		'd_request_no' => $request_no,
	    		'd_request_type' => $request->doc_request_type,
	    		'd_description' => $request->doc_description,
	    		'd_date_created' => date("Y-m-d"),
	    		'd_status' => 1
	    	]);
	    	DB::commit();
            return Response([
                'success' => true,
                'message' => "Document successfully sent",
            ],200);
		} catch (Exception $e) {
            DB::rollback();
            return Response([
                'success' => false,
                'message' => "Something went wrong",
            ],200);
	    }
	}

	public function send(Request $request) {
        // return $request->all();
		$curr_emp = Receivers::where([
			'module' => 'Documents',
			'code'=> 'CHRD'
		])->first()['employeeid'];

    	if(! $curr_emp) {
            return Response([
                'success' => false,
                'message' => "Receiver not declared.",
            ],200);
    	}
    	$document = Documents::where([
    		"d_request_no" => $request->request_no,
    		"d_current_id" => Auth::user()->id
    	])->first();
        // return $document;
		DB::beginTransaction();
		try {
            if($document->d_status == 1) {
                $document->update([
                    'd_current_id' => $curr_emp,
                    'd_status' => 2
                ]);
                $message = "Document successfully sent to CHRD";
            }else{
                $document->update([
                    'd_current_id' => $document->d_created_by,
                    'd_chrd_remarks' => $request->chrd_remarks,
                    'd_received_date' => $request->received_date,
                    'd_site' => $request->site,
                    'd_status' => 3
                ]);
                if($document->d_request_type == 1) 
                    $document->update(['d_disposal_date' => $request->disposal_date]);
                $message = "Document successfully approved";
            }
            DB::commit();
            return Response([
                'success' => true,
                'message' => $message,
            ],200);
		} catch (Exception $e) {
            DB::rollback();
            return Response([
                'success' => false,
                'message' => "Something went wrong",
            ],200);
	    }
	}
    	
}
