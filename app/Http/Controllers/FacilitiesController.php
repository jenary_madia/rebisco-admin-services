<?php

namespace App\Http\Controllers;

use App\AuditTrails;
use Illuminate\Http\Request;
use DB;
use App\Facility;
use App\Documents;
use Illuminate\Auth\Access\Response;
use App\Http\Requests\FacilityRequest;
use App\Selectitems;

class FacilitiesController extends Controller
{
    public $audit_trail;

    public function __construct(AuditTrails $auditTrail) {
        $this->audit_trail = $auditTrail;
    }

    public function index()
    {
        $types = Selectitems::where([
            "module" => "facilities",
            "modulesfield" => "type"
        ])->pluck("item","text");
// Reception
// Pantry
        $facilities = Facility::with("type")->orderBy("fa_id","DESC")->get();
        $documents = Documents::forApproval(2);
        $sites = Selectitems::where(["module" => "storage-disposal",
                "modulesfield" => "site"])->pluck("text","item");
        $areas = Selectitems::where([
            "module" => "facilities",
            "modulesfield" => "area"
        ])->pluck("item","text");
    	return view('facilities.index',compact('facilities','documents','types','sites','areas'));
    }

    public function addFacility(FacilityRequest $request) {
    	DB::beginTransaction();
    	try {
            if($request->fa_area == "others") {
                $area =  Selectitems::where([
                    "module" => "facilities",
                    "modulesfield" => "area"
                ])->max("item") + 1;

                Selectitems::create([
                    "module" => "facilities",
                    "modulesfield" => "area",
                    "item" => $area,
                    "text" => $request->other_area
                ]);

            }else{
                $area = $request->fa_area;
            }
            $params = [
                "fa_name" => $request->fa_name,
                "fa_type" => $request->fa_type,
                "fa_acquisition_date" => $request->fa_acquisition_date,
                "fa_retirement_date" => $request->fa_retirement_date,
                "fa_site" => $request->fa_site,
                "fa_area" => $area,
                "fa_asset_number" => $request->fa_asset_number, 
            ];
    		$facility = Facility::create($params);
            $this->audit_trail->AU001($facility->fa_id,'facility','fa_id',json_encode($params),3);
    		DB::commit();
            return Response([
                'success' => true,
                'message' => "Facility successfully added",
            ],200);
    	} catch (Exception $e) {
    		DB::rollback();
            return Response([
                'success' => false,
                'message' => "Something went wrong",
            ],200);
    	}
    }

    public function updateFacility(request $request,$id) {
        DB::beginTransaction();
        try {
            $facility = Facility::find($id);
            $old = json_encode($facility);

            if($request->fa_area == "others") {
                $area =  Selectitems::where([
                    "module" => "facilities",
                    "modulesfield" => "area"
                ])->max("item") + 1;

                Selectitems::create([
                    "module" => "facilities",
                    "modulesfield" => "area",
                    "item" => $area,
                    "text" => $request->other_area
                ]);

            }else{
                $area = $request->fa_area;
            }


            $params = [
                "fa_name" => $request->fa_name,
                "fa_type" => $request->fa_type,
                "fa_acquisition_date" => $request->fa_acquisition_date,
                "fa_retirement_date" => $request->fa_retirement_date,
                "fa_site" => $request->fa_site,
                "fa_area" => $area,
                "fa_asset_number" => $request->fa_asset_number, 
            ];
            $facility->update($params);
            $this->audit_trail->AU004($id,'facility','fa_id',json_encode($params),$old,3);
            DB::commit();
            return Response([
                'success' => true,
                'message' => "Event successfully updated",
            ],200);
        } catch (Exception $e) {
            DB::rollback();
            return Response([
                'success' => false,
                'message' => "Something went wrong",
            ],200);
        }
    }
}
