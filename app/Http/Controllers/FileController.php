<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class FileController extends Controller
{
    public function upload(Request $request) {
    	$images = [];
        foreach ($request->file('images') as $key) {
            $hashName = pathinfo($key->hashName(), PATHINFO_FILENAME);
            $path = $key->storeAs('public/uploads',$hashName);
            array_push($images,[
                "module" => "events",
                "type" => $request->attachment_type,
                "filesize" => $key->getClientSize(),
                "original_extension" => $key->extension(),
                "original_filename" => $key->getClientOriginalName(),
                "random_filename" => $hashName
            ]);

        }
        return $images;
    }
}
