<?php

namespace App\Http\Controllers;

use App\Events;
use App\Documents;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $events = Events::forAttendance();
        $documents = Documents::forApproval(1);
        return view('home',compact("events","documents"));
    }
}
