<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Documents;
use Auth;
use DB;
use App\Selectitems;

class EssController extends Controller
{
    public function index() {
    	return view("ess.dashboard");
    }

    public function requestDocument() {
    	return view("ess.documents.request");
    }

    public function document($id) {
    	$document = Documents::where([
    		"d_id" => $id,
    		"d_current_id" => Auth::user()->id 
    	])->with("owner")->first();
    	if(! $document) {
    		return redirect('home');
    	}
    	$sites = Selectitems::where(["module" => "storage-disposal",
				"modulesfield" => "site"])->pluck("text","item");
    	// return $sites;
    	return view("ess.documents.view",compact("document","sites"));
    }

}
