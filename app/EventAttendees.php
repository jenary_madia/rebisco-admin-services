<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class EventAttendees extends Model
{
    protected $table = "event_attendees";
    protected $guarded = [];
    public $timestamps = false;

    public function employee() {
    	return $this->hasOne("App\Employees",'id', 'ea_employee_id')->selectRaw("firstname+' '+middlename+' '+lastname as fullname,id,departmentid2")->with('department');
    }
}
