<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use DB;

class Events extends Model
{
    protected $table = "events";
    protected $guarded = [];
    protected $primaryKey = "ev_id";
    public $timestamps = false;

    public function eventDetails() {
    	return $this->hasMany('App\EventDetails','ed_ev_id');
    }

    public function attachments() {
    	return $this->hasMany('App\Attachments','module_id')->where('module','events');
    }

    public function invitation() {
        return $this->hasOne('App\Attachments','module_id')->where([
            'module' => 'events',
            'type' => 1,
        ]);
    }

    public function attendees() {
        return $this->hasMany('App\EventAttendees','ea_ev_id')->with("employee");
    }

    public function remarks() {
        return $this->hasMany('App\EventRemarks','er_ev_id')->join("employees","event_remarks.er_emp_id","employees.id")
        ->selectRaw("event_remarks.*,employees.firstname+' '+employees.lastname as emp_fullname");
    }

    public static function forAttendance() {
        return DB::table('events')->whereRaw("ev_id not in (select ea_ev_id from event_attendees where ea_employee_id = 61)")->get();
    }

    public function total() {
        return $this->hasOne('App\EventDetails','ed_ev_id')->select(DB::raw('SUM(event_details.ed_budget) as expenses,SUM(event_details.ed_actual) as actual'),'ed_ev_id')->groupBy("ed_ev_id");

    }
}
