<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SelectItems extends Model
{
    protected $table = "selectitems";
    protected $guarded = [];
    public $timestamps = false;
}
