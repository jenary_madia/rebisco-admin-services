<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Departments extends Model
{
    protected $table = "hrms_departments";
    protected $guarded = [];
    public $timestamps = false;
}
