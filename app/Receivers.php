<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Receivers extends Model
{
    protected $table = "receivers";
    protected $guarded = [];
    public $timestamps = false;
}
