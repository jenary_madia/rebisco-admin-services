<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;

class Employees extends Authenticatable
{
    use Notifiable;

    protected $table = "employees";
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'username', 'password',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password',
    ];

    public $remember_token = false;

    public function setPasswordAttribute($value)
    {
        $this->attributes['password'] = md5($value);
    }

    public function department() {
        return $this->hasone("App\Departments",'dept_code','departmentid2')->select("dept_code","dept_name");
    }

    public function get_dept_head($dept) {
        return $this->where([
            "departmentid2" => $dept,
            "active" => 1,
            "desig_level" => "head",
        ])->first()['id'];
        // return $this->hasone("App\Employees as dept_head",'dept_head.id','employees.id');
    }
}
