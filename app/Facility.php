<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Facility extends Model
{
    protected $table = "facility";
    protected $guarded = [];
    public $timestamps = false;
    protected $primaryKey = "fa_id";

    public function type() {
    	return $this->hasOne("App\Selectitems","item","fa_type")->where([
    		"module" => "facilities",
    		"modulesfield" => "type",
    	])->select("item","text");
    }
}
