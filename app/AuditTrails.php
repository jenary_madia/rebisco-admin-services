<?php

namespace App;
use Auth;

use Illuminate\Database\Eloquent\Model;

class AuditTrails extends Model
{
    protected $table = "audit_trails";
    protected $guarded = [];
    public $timestamps = false;

    public function AU001($id,$tableName,$primaryKey,$new,$module_id)
    {
        AuditTrails::create([
            "user_id" => Auth::user()->id,
            "date" => date('Y-m-d'),
            "time" => date('H:i:s'),
            "action_code" => 'AU001',
            "module_id" => $module_id,
            "table_name" => $tableName,
            "params" =>  json_encode(array("{$tableName}.{$primaryKey}"=>$id)),
            "new" => $new,
        ]);
    }

    //Upload File
    public function AU002($id,$tableName,$primaryKey,$new,$module_id)
    {
        AuditTrails::create([
            "user_id" => Auth::user()->id,
            "date" => date('Y-m-d'),
            "time" => date('H:i:s'),
            "action_code" => 'AU002',
            "module_id" => $module_id,
            "table_name" => $tableName,
            "params" =>  json_encode(array("{$tableName}.{$primaryKey}"=>$id)),
            "new" => $new,
        ]);
        
    }

    //Retrieve
    public function AU003($id,$tableName,$primaryKey,$module_id)
    {
        AuditTrails::create([
            "user_id" => Auth::user()->id,
            "date" => date('Y-m-d'),
            "time" => date('H:i:s'),
            "action_code" => 'AU003',
            "module_id" => $module_id,
            "table_name" => $tableName,
            "params" =>  json_encode(array("{$tableName}.{$primaryKey}"=>$id)),
        ]);
        
    }

    //Edit and Update(workflow of request)
    public function AU004($id,$tableName,$primaryKey,$new,$old,$module_id)
    {

        AuditTrails::create([
            "user_id" => Auth::user()->id,
            "date" => date('Y-m-d'),
            "time" => date('H:i:s'),
            "action_code" => 'AU004',
            "module_id" => $module_id,
            "table_name" => $tableName,
            "params" =>  json_encode(array("{$tableName}.{$primaryKey}"=>$id)),
            "new" => $new,
            "old" => $old
        ]);
        
    }

    //Download
    public function AU008($id,$tableName,$primaryKey,$module_id)
    {
         AuditTrails::create([
            "user_id" => Auth::user()->id,
            "date" => date('Y-m-d'),
            "time" => date('H:i:s'),
            "action_code" => 'AU008',
            "module_id" => $module_id,
            "table_name" => $tableName,
            "params" =>  json_encode(array("{$tableName}.{$primaryKey}"=>$id)),
        ]);
        
    }

}
