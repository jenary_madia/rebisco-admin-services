$(document).ready(function () {
	$("#fa_area").on("change",function() {
		if($(this).val() == "others") {
			$("#other_area").show();
		}else{
			$("#other_area").hide();
		}
	});
	$("#add-facility").on("submit", function(e) {
		e.preventDefault();
		var form_data = $(this).serialize();
		axios.post(base_url+'/facilities/add', form_data)
		.then(function (response) {
			if(response.data.success) {
				alert(response.data.message);
				setTimeout(function() {
					location.reload();
				},1000);
			}
			
		})
		.catch(function (error) {
			console.log(error);
		});
	});

	$("#reset-facility").on("click", function(e) {
		e.preventDefault();
		$('#add-facility').trigger("reset");
	});

	$('.datepicker').datepicker({
        dateFormat : 'yy-mm-dd'
    });

    $('#list-furnitures').DataTable({
    	"lengthMenu": [[5, 10, 15, -1], [5, 10, 15, "All"]]
    });

    var to_edit_id = "";

    $('body').on('dblclick', '.facility_row', function(e) {
		var data = $(this).data("json");
		$("#fa_name").val(data.fa_name);
		$("#fa_type").val(data.fa_type);
		$("#fa_acquisition_date").val(data.fa_acquisition_date);
		$("#fa_retirement_date").val(data.fa_retirement_date);
		$("#fa_site").val(data.fa_site);
		$("#fa_area").val(data.fa_area);
		$("#fa_asset_number").val(data.fa_asset_number);
		to_edit_id = data.fa_id;
		$(".btn-save").hide();
		$(".btn-update").show();
	});

	$(".btn-update").on("click", function(e) {
		e.preventDefault();
		var form_data = $("#add-facility").serialize();
		console.log(form_data);
		axios.post(base_url+'/facility/'+to_edit_id, form_data)
		.then(function (response) {
			alert(response.data.message);
			setTimeout(function() {
				location.reload();
			},1000);
		})
		.catch(function (error) {
			console.log(error);
		});
	});

});

