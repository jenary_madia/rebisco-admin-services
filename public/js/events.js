$(document).ready(function() {

	$('#list-events').DataTable({
		"bLengthChange": false,
        "info": false,
        "searching": false
	});

	$('.datepicker').datepicker({
        dateFormat : 'yy-mm-dd'
    });


	var totals = {
		venue : 0,
		food_beverages : 0,
		prizes : 0,
		performers : 0,
		logistics : 0
	};

	var total_actual = 0;


	if (typeof(ex_total_actual) !== "undefined") {
		total_actual = ex_total_actual;
	}

	$(document).on('keypress', '.numeric', function (e) {
        return isNumeric(e);
    });

	function isNumeric(e) {
        if (e.shiftKey || (e.keyCode < 45 || e.keyCode > 57) || e.keyCode == 46  || e.keyCode == 47) {
            e.preventDefault();
        }
    }

	$("#add-event-details").on("click",function(e) {
		e.preventDefault();
		var category = $("#ed_category").val();
		var supplier = $("#ed_supplier").val();
		var budget = $("#ed_budget").val();
		if(! supplier || ! category) {
			alert("Please input required fields");
			return false;
		}

		if (budget.match(/[a-z]/i)) {
		   alert("Budget must be numeric.");
			return false;
		}

		if(category == "others") 
			$(".tbl-others").show();
		categoryListing("add",category,supplier,budget);		
		
	});

	function categoryListing(action,category,supplier,budget,actual = 0) {
		switch(category) {
			case "venue" :
				appendThis(action,$(".tbl-body-venue"),supplier,budget,category,actual);
			break;

			case "food_beverages" :
				appendThis(action,$(".tbl-body-food_beverages"),supplier,budget,category,actual);
			break;

			case "prizes" :
				appendThis(action,$(".tbl-body-prizes"),supplier,budget,category,actual);
			break;

			case "performers" :
				appendThis(action,$(".tbl-body-performers"),supplier,budget,category,actual);
			break;
				
			case "logistics" :
				appendThis(action,$(".tbl-body-logistics"),supplier,budget,category,actual);
			break;

			case "others" :
				appendThis(action,$(".tbl-body-others"),supplier,budget,category,actual);
			break;
		}
		$("#ed_supplier").val("");
		$("#ed_budget").val("");
		var current_total = $("#total_budget").html() ? $("#total_budget").html() : 0; 
		var total = parseInt(current_total) + parseInt(budget);
		$("#total_budget").html(total);
	}

	function gatherEventDetails() {
		var event_details = {
			venue : [],
			food_beverages : [],
			prizes : [],
			performers : [],
			logistics : [],
			others : []
		};
		$(".tbl-body-venue").find("tr").each(function() {
			var params = {};
			$(this).find("td").each(function() {
				var input = $(this).find("input");
				if(input.data("name"))
					params[input.data("name")] = input.val();
			});
			params['ed_category'] = 'venue';
			event_details.venue.push(params);
		});

		$(".tbl-body-food_beverages").find("tr").each(function() {
			var params = {};
			$(this).find("td").each(function() {
				var input = $(this).find("input");
				if(input.data("name"))
					params[input.data("name")] = input.val();
			});
			params['ed_category'] = 'food_beverages';
			event_details.food_beverages.push(params);
		});

		$(".tbl-body-prizes").find("tr").each(function() {
			var params = {};
			$(this).find("td").each(function() {
				var input = $(this).find("input");
				if(input.data("name"))
					params[input.data("name")] = input.val();
			});
			params['ed_category'] = 'prizes';
			event_details.prizes.push(params);
		});

		$(".tbl-body-performers").find("tr").each(function() {
			var params = {};
			$(this).find("td").each(function() {
				var input = $(this).find("input");
				if(input.data("name"))
					params[input.data("name")] = input.val();
			});
			params['ed_category'] = 'performers';
			event_details.performers.push(params);
		});

		$(".tbl-body-logistics").find("tr").each(function() {
			var params = {};
			$(this).find("td").each(function() {
				var input = $(this).find("input");
				if(input.data("name"))
					params[input.data("name")] = input.val();
			});
			params['ed_category'] = 'logistics';
			event_details.logistics.push(params);
		});

		$(".tbl-body-others").find("tr").each(function() {
			var params = {};
			$(this).find("td").each(function() {
				var input = $(this).find("input");
				if(input.data("name"))
					params[input.data("name")] = input.val();
			});
			params['ed_category'] = 'others';
			event_details.others.push(params);
		});

		return event_details;
	}

	// function gatherNewRemarks() {
	// 	var remarks = [];
	// 	$(".added-comment").each(function() {
	// 		remarks.push($(this).html());
	// 	});

	// 	return remarks;
	// }


	$("#event-create").on("submit", function(e) {
		e.preventDefault();
		var event_details = gatherEventDetails();
		console.log(event_details);
        axios.post(base_url+'/events/add', {
			'ev_name' : $('#ev_name').val(),
			'ev_date' : $('#ev_date').val(),
			'ev_description' : $('#ev_description').val(),
			'ev_invite_date' : $('#ev_invite_date').val(),
			'event_details' : event_details,
			'attachments' : getAttachments()
        })
        .then(function (response) {
			if(response.data.success == true) {
        		alert(response.data.message);
				if(response.data.success) {
					setTimeout(function() {
						location.reload();
					},1000);
				}
        	}else{
        		var error_message = "";
        		for (var i = 0; i < response.data.errors.length; i++) {
        			error_message += response.data.errors[i]+"\n";
        		}
        		alert(error_message);
        	}	
		})
		.catch(function (error) {
		});
	});

	$('body').on('blur', '.actual', function() {
		if(typeof $(this).attr('readonly') == "undefined") {
			if ($(this).val().match(/[a-z]/i)) {
				return false;
			}else{
				// if($(this).val().length > 0 ) {
				// 	total_actual += parseInt($(this).val());
				// }
				var total = 0;
				$(".actual").each(function() {
					total = total + parseInt($(this).val());
				});
			}
			$("#total_actual").html(total);
		}
	});

	$('body').on('focus', '.actual', function() {
		if(typeof $(this).attr('readonly') == "undefined") {
			if ($(this).val().match(/[a-z]/i)) {
				return false;
			}else{
				var total = 0;
				$(".actual").each(function() {
					total = total + parseInt($(this).val());
				});

				if($(this).val().length != 0 || $(this).val() != 0  ) {
					total = total - parseInt($(this).val());
				}
				$("#total_actual").html(total);
			}
			$("#total_actual").html(total);
		}
	});

	$('body').on('blur', '.input-budget', function() {
		if(typeof $(this).attr('readonly') == "undefined") {
			if ($(this).val().match(/[a-z]/i)) {
				return false;
			}else{
				// if($(this).val().length > 0 ) {
				// 	total_actual += parseInt($(this).val());
				// }
				var total = 0;
				$(".input-budget").each(function() {
					total = total + parseInt($(this).val());
				});
			}
			$("#total_budget").html(total);
		}
	});

	$('body').on('focus', '.input-budget', function() {
		if(typeof $(this).attr('readonly') == "undefined") {
			if ($(this).val().match(/[a-z]/i)) {
				return false;
			}else{
				var total = 0;
				$(".input-budget").each(function() {
					total = total + parseInt($(this).val());
				});

				if($(this).val().length != 0 || $(this).val() != 0  ) {
					total = total - parseInt($(this).val());
				}
				$("#total_budget").html(total);
			}
			$("#total_budget").html(total);
		}
	});

	function appendThis(action, element,supplier,budget,category,actual) {
		if(action == "add") {
			var status = "readonly";
		}else{
			var status = "readonly";
		}
		element.append("<tr data-category='"+category+"'><td><input data-name='ed_supplier' readonly value='"+supplier+"' type='text' class='input_list form-control input-sm' style='width:100px'></td><td><input data-name='ed_budget' readonly value='"+budget+"' type='text' class='input_list form-control input-sm input-budget' style='width:100px'></td><td><input "+status+" data-name='ed_actual' value='"+actual+"' type='text' data-category='"+category+"' class='form-control input-sm actual numeric' style='width:50px' maxlength='2'></td><td><button class='delete-icon container-icon'><span class='glyphicon glyphicon-remove'></span></button><button class='edit-icon container-icon' type='button'><span class='glyphicon glyphicon-pencil'></span></button></td></tr>")
	}

	$('body').on("dblclick",".event_row",function() {
		var id = $(this).data("id");
		window.location = base_url+'/event/'+id;
	})

	$("#event-edit").on("submit", function(e) {
		e.preventDefault();
		var event_details = gatherEventDetails();
		var new_comment = $(".new-comment").val();
        axios.post(base_url+'/event/'+ev_id, {
			'ev_name' : $('#ev_name').val(),
			'ev_date' : $('#ev_date').val(),
			'ev_description' : $('#ev_description').val(),
			'ev_invite_date' : $('#ev_invite_date').val(),
			'event_details' : event_details,
			'comment' : new_comment,
			'attachments' : getAttachments()
        })
        .then(function (response) {
			if(response.data.success == true) {
        		alert(response.data.message);
				if(response.data.success) {
					setTimeout(function() {
						location.reload();
					},1000);
				}
        	}else{
        		var error_message = "";
        		for (var i = 0; i < response.data.errors.length; i++) {
        			error_message += response.data.errors[i]+"\n";
        		}
        		alert(error_message);
        	}
		})
		.catch(function (error) {
		});
	});

	$('body').on('click','.edit-icon',function() {
		var tr = $(this).closest("tr");
		tr.find('td').each(function (key, val) {
			$(this).find("input").removeAttr("readonly");
        });
	});

	$('body').on('click','.delete-icon',function(e) {
		e.preventDefault();
		var tr = $(this).closest("tr");
		tr.remove();
	});

	$('body').on('change', '#browse-image', function(e) {
		let files = e.target.files || e.dataTransfer.files;
        let data = new FormData();
        data.append('attachment_type',0);
        for (var i = 0; i < files.length; i++) {
            data.append('images[' + i + ']', files.item(i), files.item(i).name);
        }
        const config = {
            headers: { 'content-type': 'multipart/form-data' }
        }
        axios.post(base_url+'/file-upload', data, config)
        .then(function (response) {
        	console.log(response.data);
        	for (var i = 0; i < response.data.length; i++) {
        		var image = response.data[i]
        		$('.attachments').append("<li><input type='hidden' value='"+JSON.stringify(image)+"' class='input-attachment'>"+image.original_filename+"<button class='btn btn-xs btn-danger'>DELETE</button></li>");
        	}
		})
		.catch(function (error) {
		});
	});

	$('body').on('change', '#browse-invite', function(e) {
		if($(".invite").children().length >= 1){
			alert("1 attachment is allowed");
			return false;
		}
		let files = e.target.files || e.dataTransfer.files;
        let data = new FormData();
        data.append('attachment_type',1);
        for (var i = 0; i < files.length; i++) {
            data.append('images[' + i + ']', files.item(i), files.item(i).name);
        }
        const config = {
            headers: { 'content-type': 'multipart/form-data' }
        }
        axios.post(base_url+'/file-upload', data, config)
        .then(function (response) {
        	for (var i = 0; i < response.data.length; i++) {
        		var image = response.data[i]
        		$('.invite').append("<li><input type='hidden' value='"+JSON.stringify(image)+"' class='input-attachment'>"+image.original_filename+"<button class='btn btn-xs btn-danger'>DELETE</button></li>");
        	}
        	console.log(attachments);
		})
		.catch(function (error) {
		});
	});

	$('body').on('click', '.attachments button,.invite button', function(e) {
		e.preventDefault();
		$(this).parent().remove();
	})

	function getAttachments() {
		var attachments = [];
		$(".input-attachment").each(function() {
		    attachments.push(JSON.parse($(this).val()));
		});
		return attachments;
	}

})