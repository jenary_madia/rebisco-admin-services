$(document).ready(function() {
	$('.datepicker').datepicker({
        dateFormat : 'M dd, yy'
    });

    $('.timepicker').timepicker({
    	defaultTime: '',
    	useCurrent: false
    });
	var out_list = $('#list_outgoing').DataTable({
		"bLengthChange": false,
		"lengthMenu": [[5, 10, 15, -1], [5, 10, 15, "All"]],
        "info": false,
        "searching": false
	});

	logsheets = JSON.parse(outgoing_logs);
	addRows(logsheets);

	employees = JSON.parse(employees);
	var options = "";
	for (var i = 0; i < employees.length; i++) {
		options+="<option value='"+employees[i]['id']+"'>"+employees[i]['fullname']+"</option>"
	}

	var select_name = "<select data-type='name' class='form-control input-xs select_employees'><option value=''></option></select>";
	var select_recipient = "<select data-type='recipient' class='form-control input-xs select_employees'><option value=''></option></select>";
	var for_edit = [];
	var for_add = [];

	$('body').on("click","#btn-add-outgoing" ,function() {
		for_add.push({
			"ls_sender_id" : "",
			"ls_receiver_id" : "",
			"ls_particulars" : "",
		});
		var new_row = out_list.row.add([
			select_name,
			"<input type='text' class='form-control input-sm' readonly>",
			"<input data-type='particulars' maxlength='150' type='text' class='form-control input-sm' maxlength='150'>",
			select_recipient,
			"<input type='text' class='form-control input-sm' readonly>",
			"<a data-index='"+(for_add.length - 1)+"' class='delete-icon container-icon'><span class='glyphicon glyphicon-remove'></span></a>",
		]).draw(false);
        out_list.order([1, 'desc']).draw();
        out_list.page('last').draw(false);
		$(".select_employees").append(options);
		initiate_chosen();
    	new_row.nodes().to$().attr('data-id',for_add.length - 1);
    	new_row.nodes().to$().find(":input").addClass("for-add");
    	$(".btn-save-outgoing").show();

    	$(".btn-save-outgoing").show();
    	$(".btn-reset-outgoing").show();
	})

	function initiate_chosen() {
		$(".select_employees").chosen({
			no_results_text: "Oops, nothing found!"
		}); 
		$(".select_employees").chosen().change(function() {
			$(this).closest("td").next().find("input").val(get_deptcode($(this).val()));
			if($(this).data("type") == "recipient") {
				setTimeout(function() { 
					$('#btn-add-outgoing').focus(); 
				}, 100);
			}
			
		})
	} 

	function get_deptcode(employeeid) {
		for (var i = 0; i < employees.length; i++) {
			if(employees[i]['id'] == employeeid)
				return employees[i]['departmentid2'];
		}
	}

	$('.btn-save-outgoing').on("click",function() {
		var data_filter = {};
		var filters = $("#filter_form").serializeArray();
		for (var i = 0; i < filters.length; i++) {
			data_filter[filters[i]["name"]] = filters[i]["value"];
		}
		axios.post(base_url+'/document-logsheet/save', {
			new_data : for_add,
			edited_data : for_edit,
			other_data : data_filter
		})
        .then(function (response) {
        	if(response.data.success == true) {
        		alert(response.data.message);
				if(response.data.success) {
					setTimeout(function() {
						location.reload();
					},1000);
				}
        	}else{
        		var error_message = "";
        		for (var i = 0; i < response.data.errors.length; i++) {
        			error_message += response.data.errors[i]+"\n";
        		}
        		alert(error_message);
        	}	
			
		})
		.catch(function (error) {
		});
	})

	$('.btn-reset-outgoing').on("click",function() {
		addRows(logsheets);
	});

	$('body').on("click","#btn-filter-outgoing" ,function(e) {
		e.preventDefault();
		var data = $("#filter_form").serialize();

		axios.post(base_url+'/document-logsheet/filter/outgoing', data)
        .then(function (response) {
        	addRows(response.data);
		})
		.catch(function (error) {
		});
	})


	$('body').on('click','.edit-icon',function() {
		var ls_id = $(this).val();
		var tr = $(this).closest("tr");
        for (var i = 0; i < logsheets.length; i++) {
        	if(logsheets[i]['ls_id'] == ls_id) {
        		var data = logsheets[i];
        	}
        }

        for_edit.push(data);

     	out_list.row(tr).data([
        	select_name,
        	"<input value='"+data['sender_dept']+"' type='text' class='form-control input-sm' readonly>",
        	"<input data-type='particulars' maxlength='150' value='"+data['ls_particulars']+"' type='text' class='form-control input-sm'>",
        	select_recipient,
        	"<input value='"+data['receiver_dept']+"' type='text' class='form-control input-sm' readonly>",
        	"<button style='margin-right: 5px;' value='"+data['ls_id']+"' class='delete-icon container-icon'><span class='glyphicon glyphicon-remove'></span></button><button data-type='log_id' value='"+data['ls_id']+"' class='edit-icon container-icon' type='button'><i class='glyphicon glyphicon-pencil'></i></button>",
    	]);

    	$(".select_employees").append(options);
		$('.select_employees[data-type="name"]').val(data['ls_sender_id']);
		$('.select_employees[data-type="recipient"]').val(data['ls_receiver_id']);
    	initiate_chosen();
		tr.find(":input").addClass("for-edit");
		tr.attr("data-id",for_edit.length - 1);
    	out_list.draw(false);
    	$(".btn-save-outgoing").show();

    	$(".btn-save-outgoing").show();
    	$(".btn-reset-outgoing").show();
	})

	$('body').on('change','.for-edit',function() {
		var tr = $(this).closest("tr");
		var ls_id = tr.data("id");
		tr.find(":input").each(function() {
			switch($(this).data("type")) {
				case "name":
					for_edit[ls_id].ls_sender_id = $(this).val();
				break;

				case "recipient":
					for_edit[ls_id].ls_receiver_id = $(this).val();
				break;

				case "particulars":
					for_edit[ls_id].ls_particulars = $(this).val();
					$('#btn-add-outgoing').focus(); 
				break;
			}
		});
	})

	$('body').on('change','.for-add',function() {
		var tr = $(this).closest("tr");
		var index = tr.data("id");
		tr.find(":input").each(function() {
			switch($(this).data("type")) {
				case "name":
					for_add[index].ls_sender_id = $(this).val();
				break;

				case "recipient":
					for_add[index].ls_receiver_id = $(this).val();
				break;

				case "particulars":
					for_add[index].ls_particulars = $(this).val();
				break;
			}
		});
	})

	$('body').on('click','.delete-icon',function() {
		if(confirm("Are you sure you want to delete this record?")) {
		    out_list
	        .row( $(this).parents('tr') )
	        .remove()
	        .draw();
	        var ls_id = $(this).val();
	        if(ls_id) {
	        	axios.post(base_url+'/document-logsheet/delete', {
	        		'ls_id' : ls_id
	        	})
		        .then(function (response) {
					alert(response.data.message);
				})
				.catch(function (error) {
				});
	        }else{
	        	
	        	for_add.splice($(this).data("index"),1);
	        	console.log(for_add);
	        }
		}
	})

	function addRows(data) {
		out_list.clear()
		var to_display = [];
		for (var i = 0; i < data.length; i++) {
			to_display.push([
				"<input value='"+data[i]['sender_fullname']+"' type='text' class='form-control input-sm' readonly>",
				"<input value='"+data[i]['sender_dept']+"' type='text' class='form-control input-sm' readonly>",
				"<input value='"+data[i]['ls_particulars']+"' data-type='particulars' type='text' class='form-control input-sm' readonly>",
				"<input value='"+data[i]['receiver_fullname']+"' type='text' class='form-control input-sm' readonly>",
				"<input value='"+data[i]['receiver_dept']+"' type='text' class='form-control input-sm' readonly>",
				"<button style='margin-right: 5px;' value='"+data[i]['ls_id']+"' class='delete-icon container-icon'><i class='text-danger glyphicon glyphicon-remove'></i></button><button data-type='log_id' value='"+data[i]['ls_id']+"' class='edit-icon container-icon' type='button'><i class='glyphicon glyphicon-pencil'></span></button>",
			])
		}
		var rows = out_list.rows.add(to_display).draw().nodes();
		$(rows).addClass('existing_log');
	}

	//*********************** Incoming

	var incoming_list = $('#list_incoming').DataTable({
		"bLengthChange": false,
		"lengthMenu": [[5, 10, 15, -1], [5, 10, 15, "All"]],
        "info": false,
        "searching": false
	});
	var to_received = [];
	$('body').on('change','.incoming_cbox',function() {
		if ($(this).prop("checked")) {
	        to_received.push($(this).val());
	    }else{
	    	var index = to_received.indexOf($(this).val());
	    	to_received.splice(index,1);
	    }
	    if(to_received.length >= 1) {
	    	$(".btn-receive-incoming").show();
	    }else{
	    	$(".btn-receive-incoming").hide();
	    }
	})

	$('.btn-receive-incoming').on("click",function() {
		
		axios.post(base_url+'/document-logsheet/receive', to_received)
        .then(function (response) {
			alert(response.data.message);
			if(response.data.success) {
				setTimeout(function() {
					location.reload();
				},1000);
			}
			
		})
		.catch(function (error) {
		});
	})

	$('body').on("click","#btn-filter-incoming" ,function(e) {
		e.preventDefault();
		var data = $("#filter_form_incoming").serialize();
		axios.post(base_url+'/document-logsheet/filter/incoming', data)
        .then(function (response) {
        	incoming_list.clear();
        	var data = response.data;
			var to_display = [];
			for (var i = 0; i < data.length; i++) {
				to_display.push([
					"<input value='"+data[i]['sender_fullname']+"' type='text' class='form-control input-sm' readonly>",
					"<input value='"+data[i]['sender_dept']+"' type='text' class='form-control input-sm' readonly>",
					"<input value='"+data[i]['ls_particulars']+"' data-type='particulars' type='text' class='form-control input-sm' readonly>",
					"<input value='"+data[i]['receiver_fullname']+"' type='text' class='form-control input-sm' readonly>",
					"<input value='"+data[i]['receiver_dept']+"' type='text' class='form-control input-sm' readonly>",
					'<input class="incoming_cbox" type="checkbox" value="'+data[i]['ls_id']+'" >',
				])
			}
			incoming_list.rows.add(to_display).draw().nodes();
		})
		.catch(function (error) {
		});
	})

	//**************************** report
	var report_list = $('#list_report').DataTable({
		"bLengthChange": false,
		"lengthMenu": [[5, 10, 15, -1], [5, 10, 15, "All"]],
        "info": false,
        "searching": false
	});

	$("#filter_form_report input,select").on("change",function(e){
		e.preventDefault();
		var data = $("#filter_form_report").serialize();
		console.log(data);
		axios.post(base_url+'/document-logsheet/filter/report', data)
        .then(function (response) {
        	report_list.clear();
        	var data = response.data;
			var to_display = [];
			for (var i = 0; i < data.length; i++) {
				var received_by = data[i]['received_by'] ? data[i]['received_by'] : ' ';
				to_display.push([
					"<input value='"+data[i]['sender_fullname']+"' type='text' class='form-control input-sm' readonly>",
					"<input value='"+data[i]['sender_dept']+"' type='text' class='form-control input-sm' readonly>",
					"<input value='"+data[i]['ls_particulars']+"' data-type='particulars' type='text' class='form-control input-sm' readonly>",
					"<input value='"+data[i]['receiver_fullname']+"' type='text' class='form-control input-sm' readonly>",
					"<input value='"+data[i]['receiver_dept']+"' type='text' class='form-control input-sm' readonly>",
					"<input value='"+ received_by +"' type='text' class='form-control input-sm' readonly>",
				])
			}
			report_list.rows.add(to_display).draw().nodes();
		})
		.catch(function (error) {
		});
	});
});